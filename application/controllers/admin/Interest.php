<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Interest extends MY_Controller {

	public function __construct(){

		parent::__construct();
		auth_check(); // check login auth
		$this->rbac->check_module_access();
		$this->load->model('admin/Interest_model', 'interest_model');
	}

	public function index()
	{
		$data['title'] = 'Manage Interest Rate';
		$data['interestData'] = $this->interest_model->getInterestRate();
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/interest/manage_interest', $data);
		$this->load->view('admin/includes/_footer');
	}
	public function update_interest_rate(){
		$this->rbac->check_operation_access(); // check opration permission
		if($this->input->post('submit')){
			$this->form_validation->set_rules('actual_interest_rate', 'Acutal Interest Rate', 'trim|required');
			$this->form_validation->set_rules('today_gold_rate', 'Gold rate', 'trim|required');
			$this->form_validation->set_rules('0_3', '0-3 Month', 'trim|required');
			$this->form_validation->set_rules('3_6', '3-6 Month', 'trim|required');
			$this->form_validation->set_rules('6_12', '6-12 Month', 'trim|required');
			$this->form_validation->set_rules('12_18', '12-18 Month', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/interest/index'),'refresh');
			}
			else{
				$data = array(
					'actual_interest_rate' => $this->input->post('actual_interest_rate'),
					'today_gold_rate' => $this->input->post('today_gold_rate'),
					'0_3' => $this->input->post('0_3'),
					'3_6' => $this->input->post('3_6'),
					'6_12' => $this->input->post('6_12'),
					'12_18' => $this->input->post('12_18'),
				);
				$data = $this->security->xss_clean($data);
				
				$result = $this->interest_model->update_interest_rate($data);
				if($result){
					$this->session->set_flashdata('success', 'Interest Rate has been updated successfully!');
					redirect(base_url('admin/interest'));
				}
			}
		}
	}
}

?>	