<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends MY_Controller {

	public function __construct(){

		parent::__construct();
		auth_check(); // check login auth
		$this->rbac->check_module_access();

		$this->load->model('admin/user_model', 'user_model');
		$this->load->model('admin/Activity_model', 'activity_model');
	}

	//-----------------------------------------------------------
	public function index(){

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/users/user_list');
		$this->load->view('admin/includes/_footer');
	}
	
	public function datatable_json(){				   					   
		$records['data'] = $this->user_model->get_all_users();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = ($row['is_active'] == 1)? 'checked': '';
			$data[]= array(
				++$i,
				$row['firstname'].' '.$row['lastname'],
				$row['mobile_no'],
				date_time($row['created_at']),	
				'<input class="tgl_checkbox tgl-ios" 
				data-id="'.$row['id'].'" 
				id="cb_'.$row['id'].'"
				type="checkbox"  
				'.$status.'><label for="cb_'.$row['id'].'"></label>',		

				'<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/users/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<button title="Delete" class="delete btn btn-sm btn-danger" attr="'.$row['id'].'"> <i class="fa fa-trash-o"></i></button>'
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}
	public function checkifloanexist(){
		$id = $this->input->post('id');
		$userLoan = $this->user_model->check_if_loan_exist($id);
		$records['data']=$userLoan;
		echo json_encode($records);						   

	}
	//-----------------------------------------------------------
	function change_status()
	{   
		$this->user_model->change_status();
	}

	public function add(){
		
		$this->rbac->check_operation_access(); // check opration permission

		if($this->input->post('submit')){
			$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required|max_length[30]');
			$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required|max_length[30]');
			// $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
			$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required|max_length[15]');
			$this->form_validation->set_rules('aadhar_no', 'Aadhar Number', 'trim|required|max_length[15]');
			// $this->form_validation->set_rules('pan_no', 'PAN Number', 'trim|required|max_length[15]');
			$this->form_validation->set_rules('address', 'Address', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/users/add'),'refresh');
			}
			else{
				$contact_details = $this->input->post('contact_details');
				$nominee_details = $this->input->post('nominee_details');
				$banking_details = $this->input->post('banking_details');
				$data = array(
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'email' => $this->input->post('email'),
					'mobile_no' => $this->input->post('mobile_no'),
					'id_type' => $this->input->post('id_type'),
					'id_name' => $this->input->post('id_name'),
					'aadhar_no' => $this->input->post('aadhar_no'),
					'pan_no' => $this->input->post('pan_no'),
					'contact_details' => $this->input->post('contact_details'),
					'nominee_details' => $this->input->post('nominee_details'),
					'banking_details' => $this->input->post('banking_details'),
					'address' => $this->input->post('address'),
					'image' => $this->input->post('imgData'),
					// 'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
					'created_at' => date('Y-m-d : h:m:s'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);
				if($contact_details == 1){
					$data['cname'] = $this->input->post('cname');
					$data['cno']   = $this->input->post('cno');
				}
				if($nominee_details == 1){
					$data['nom_name']     = $this->input->post('nom_name');
					$data['nom_rel']   = $this->input->post('nom_rel');
				}
				if($banking_details == 1){
					$data['bankname'] = $this->input->post('bankname');
					$data['acc_no']   = $this->input->post('acc_no');
					$data['ifsc']     = $this->input->post('ifsc');
					$data['branch']   = $this->input->post('branch');
				}
				// $data = $this->security->xss_clean($data);
				$result = $this->user_model->add_user($data);
				if($result){

					// Activity Log 
					$this->activity_model->add_log(1);

					$this->session->set_flashdata('success', 'User has been added successfully!');
					redirect(base_url('admin/users'));
				}
			}
		}
		else{
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/users/user_add');
			$this->load->view('admin/includes/_footer');
		}
		
	}

	public function edit($id = 0){

		$this->rbac->check_operation_access(); // check opration permission

		if($this->input->post('submit')){
			$this->form_validation->set_rules('firstname', 'Username', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
			$this->form_validation->set_rules('mobile_no', 'Number', 'trim|required');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
					$data = array(
						'errors' => validation_errors()
					);
					$this->session->set_flashdata('errors', $data['errors']);
					redirect(base_url('admin/users/user_edit/'.$id),'refresh');
			}
			else{
				$contact_details = $this->input->post('contact_details');
				$nominee_details = $this->input->post('nominee_details');
				$banking_details = $this->input->post('banking_details');
				$data = array(
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'email' => $this->input->post('email'),
					'mobile_no' => $this->input->post('mobile_no'),
					'id_type' => $this->input->post('id_type'),
					'id_name' => $this->input->post('id_name'),
					'aadhar_no' => $this->input->post('aadhar_no'),
					'pan_no' => $this->input->post('pan_no'),
					'contact_details' => $this->input->post('contact_details'),
					'nominee_details' => $this->input->post('nominee_details'),
					'banking_details' => $this->input->post('banking_details'),
					'address' => $this->input->post('address'),
					'image' => $this->input->post('imgData'),
					'is_active' => $this->input->post('status'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);
				if($contact_details == 1){
					$data['cname'] = $this->input->post('cname');
					$data['cno']   = $this->input->post('cno');
				}else{
					$data['cname'] = '';
					$data['cno']   = '';
				}

				if($nominee_details == 1){
					$data['nom_name']  = $this->input->post('nom_name');
					$data['nom_rel']   = $this->input->post('nom_rel');
				}else{
					$data['nom_name']  = '';
					$data['nom_rel']   = '';
				}

				if($banking_details == 1){
					$data['bankname'] = $this->input->post('bankname');
					$data['acc_no']   = $this->input->post('acc_no');
					$data['ifsc']     = $this->input->post('ifsc');
					$data['branch']   = $this->input->post('branch');
				}else{
					$data['bankname'] = '';
					$data['acc_no']   = '';
					$data['ifsc']     = '';
					$data['branch']   = '';
				}
				// $data = $this->security->xss_clean($data);
				$result = $this->user_model->edit_user($data, $id);
				if($result){
					// Activity Log 
					$this->activity_model->add_log(2);

					$this->session->set_flashdata('success', 'User has been updated successfully!');
					redirect(base_url('admin/users'));
				}
			}
		}
		else{
			$data['user'] = $this->user_model->get_user_by_id($id);
			
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/users/user_edit', $data);
			$this->load->view('admin/includes/_footer');
		}
	}

	public function delete($id = 0)
	{
		$this->rbac->check_operation_access(); // check opration permission
		
		$this->db->delete('ci_users', array('id' => $id));

		// Activity Log 
		$this->activity_model->add_log(3);

		$this->session->set_flashdata('success', 'User has been deleted successfully!');
		redirect(base_url('admin/users'));
	}

}


?>