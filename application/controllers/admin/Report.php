<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Report extends MY_Controller {

	public function __construct(){

		parent::__construct();
		auth_check(); // check login auth
		$this->rbac->check_module_access();
		$this->load->library('pdf');
		// $this->load->helper('pdf_helper'); // loaded pdf helper
		$this->load->model('admin/Report_model', 'Report_model');
	}
	public function index(){
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/loan/loan_list');
		$this->load->view('admin/includes/_footer');
	}

	function change_status()
	{   
		$this->Report_model->change_status();
	}
	public function user_report(){
		$this->rbac->check_operation_access(); // check opration permission
			$data['title'] = 'Userwise Loan Report';
			$data['loanData'] = $this->Report_model->get_loan_by_id(27);
			$data['loanInventory'] = $this->Report_model->get_loan_inventory_id(27);
			$data['inventoryData'] = $this->Report_model->get_all_inventory();
			$data['users'] = $this->Report_model->get_all_users(); 
			//$data['users'] = $this->Report_model->get_user_by_id(1);
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/report/user_wise_report',$data);
			$this->load->view('admin/includes/_footer');
	}
	 public function getUserLoan()
	{
		$user_id= $this->input->get('userid');
		$data['userloanData'] = $this->Report_model->get_loan_by_userid($user_id);
		echo json_encode($data['userloanData']);
	}
	public function generate_pdf()
	{
		$this->rbac->check_operation_access(); // check opration permission
		if($this->input->post('submit')){
			$this->form_validation->set_rules('username', 'Customer Name', 'trim|required');
			$this->form_validation->set_rules('loan_id', 'Loan', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/report/user_report'),'refresh');
			}
			else{
				$username = $this->input->post('username');
				$loan_id  = $this->input->post('loan_id');
				$data['loan_id'] =$loan_id;
				$data['loanInventory'] = $this->Report_model->get_loan_inventory($loan_id);
				$data['loanInfo'] = $this->Report_model->get_loan_by_id($loan_id);
				$data['user'] = $this->Report_model->get_user_by_id($username);
				$this->load->view('admin/report/generate_pdf',$data);
				// $html = $this->output->get_output();
				// $this->load->library('pdf');
				// $this->pdf->loadHtml($html);
				// $this->pdf->setPaper('A4', 'landscape');
				// $this->pdf->render();
				// // Output the generated PDF (1 = download and 0 = preview)
				// $this->pdf->stream("html_contents.pdf", array("Attachment"=> 0));
			}
		}
	}
	public function datatable_json_fledge(){				   					   
		$records['data'] = $this->Report_model->get_all_fledge_loan();
		$data = array();

		$i=0;
		//echo '<pre>';print_r($records['data']);exit;
		foreach ($records['data']   as $row) 
		{ 
			if($row['loan_progress']=='auction')
			{
				$loan_stat ='<label style="color:red">Auction</label>';
			} 
			else if($row['loan_progress']=='ongoing')
			{
				$loan_stat ='<label style="color:green">Ongoing</label>';
			}
			else if($row['loan_progress']=='closed')
			{
				$loan_stat ='<label style="color:#17a2b8">Closed</label>';
			}
			$status = ($row['loan_status'] == 1)? 'checked': '';
			$progress = ($row['loan_progress'] == 'closed')? 'disabled': '';
			$data[]= array(
				++$i,
				$row['loan_id'],
				$row['firstname']." ".$row['lastname'],
				$row['amount'],
				date_time($row['created_on']),
				$loan_stat,
				//'<input class="tgl_checkbox tgl-ios" 
				//data-id="'.$row['loan_id'].'" 
				//id="cb_'.$row['loan_id'].'"
				//type="checkbox"  
				//'.$status.'><label for="cb_'.$row['loan_id'].'"></label>',		
				'<a title="View" class="view btn btn-sm btn-info '.$progress.'" href="'.base_url('admin/loan/view/'.$row['loan_id']).'"> <i class="fa fa-eye"></i></a>'
			);
		}
		$records['data']=$data;
		//echo '';print_r($records['data']);exit;
		echo json_encode($records);						   
	}
	public function pledge_report(){
		$search_flag = $this->input->post('flag');
		if(empty($search_flag))
		{
		$data['loanlist'] = $this->Report_model->get_all_fledge_loan();
		//echo '<pre>';print_r($data['loanlist']);exit;
		$data['start_date'] = '';
		$data['end_date']   = '';	
		}
		else
		{
		  $startdate = $this->input->post('start_date');
		  $enddate = $this->input->post('end_date');
		  $data['loanlist'] = $this->Report_model->get_all_fledge_loan_filtering($startdate,$enddate);
		  $data['start_date'] = $startdate;
		  $data['end_date']   = $enddate;
		}
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/report/fledge_loan_list',$data);
		$this->load->view('admin/includes/_footer');
	}

	public function closed_report(){
		$data['loanlist'] = $this->Report_model->get_all_closed_loan();
		foreach ($data['loanlist'] as $key => $value) {
			$loanlistwithsum = $this->Report_model->get_all_closed_loan_with_sum($value['loan_id']);
			$data['loanlist'][$key]['total_default_interest'] = (int)$loanlistwithsum['default_interest'];
		}
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/report/closed_loan_list',$data);
		$this->load->view('admin/includes/_footer');
	}
	public function loan_list_year(){
	$this->load->view('admin/includes/_header');
	$this->load->view('admin/report/loan_list_year');
	$this->load->view('admin/includes/_footer');
	}
	public function datatable_json_year(){	
	$data = array();
	$oneyearback = date('Y-m-d 00:00:00', strtotime('-1 years'));
	$limityearback = date('Y-m-d 00:00:00', strtotime('-18 months'));//1.5 yr
	$records['data'] = $this->Report_model->get_all_loan_year($oneyearback,$limityearback);
	$i=0;
	foreach ($records['data']   as $row) 
	{ 
		if($row['loan_progress']=='auction')
			{
				$loan_stat ='<label style="color:red">Auction</label>';
			} 
			else if($row['loan_progress']=='ongoing')
			{
				$loan_stat ='<label style="color:green">Ongoing</label>';
			}
			else if($row['loan_progress']=='closed')
			{
				$loan_stat ='<label style="color:#17a2b8">Closed</label>';
			} 
		$status = ($row['loan_status'] == 1)? 'checked': '';
		$data[]= array(
			++$i,
			$row['loan_id'],
			$row['firstname']." ".$row['lastname'],
			$row['amount'],
			date_time($row['created_on']),
			$loan_stat
			//'<input class="tgl_checkbox tgl-ios" 
			//data-id="'.$row['loan_id'].'" 
			//id="cb_'.$row['loan_id'].'"
			//type="checkbox"  
			//'.$status.'><label for="cb_'.$row['loan_id'].'"></label>'	

		);
	}
	$records['data']=$data;
	//echo '';print_r($records['data']);exit;
	echo json_encode($records);						   
}
}

?>	