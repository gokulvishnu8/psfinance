<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Auction extends MY_Controller {

	public function __construct(){

		parent::__construct();
		auth_check(); // check login auth
		$this->rbac->check_module_access();
		$this->load->model('admin/Auction_model', 'auction_model');
	}
	public function auction_list(){

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/auction/auction_list');
		$this->load->view('admin/includes/_footer');
	}
		public function datatable_json(){				   					   
		$records['data'] = $this->auction_model->get_all_loan();
		$data = array();

		$i=0;
		//echo '<pre>';print_r($records['data']);exit;
		foreach ($records['data']   as $row) 
		{ 
			if($row['loan_progress']=='auction')
			{
				$loan_stat ='<label style="color:red">Auction</label>';
			} 
			else if($row['loan_progress']=='ongoing')
			{
				$loan_stat ='<label style="color:green">Ongoing</label>';
			}
			else if($row['loan_progress']=='closed')
			{
				$loan_stat ='<label style="color:#17a2b8">Closed</label>';
			}
			$status = ($row['loan_status'] == 1)? 'checked': '';
			$progress = ($row['loan_progress'] == 'closed')? 'disabled': '';
			$data[]= array(
				++$i,
				$row['loan_id'],
				$row['firstname']." ".$row['lastname'],
				$row['amount'],
				date_time($row['created_on']),
				$loan_stat,
				//'<input class="tgl_checkbox tgl-ios" 
				//data-id="'.$row['loan_id'].'" 
				//id="cb_'.$row['loan_id'].'"
				//type="checkbox"  
				//'.$status.'><label for="cb_'.$row['loan_id'].'"></label>',		
				'<a title="View" class="view btn btn-sm btn-info '.$progress.'" href="'.base_url('admin/loan/view/'.$row['loan_id']).'"> <i class="fa fa-eye"></i></a>'
			);
		}
		$records['data']=$data;
		//echo '';print_r($records['data']);exit;
		echo json_encode($records);						   
	}
	function change_status()
	{   
		$this->inventory_model->change_status();
	}
	 
	
}

?>	