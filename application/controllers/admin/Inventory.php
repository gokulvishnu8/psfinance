<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Inventory extends MY_Controller {

	public function __construct(){

		parent::__construct();
		auth_check(); // check login auth
		$this->rbac->check_module_access();
		$this->load->model('admin/Inventory_model', 'inventory_model');
	}
	public function index(){

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/inventory/inventory_list');
		$this->load->view('admin/includes/_footer');
	}
	public function datatable_json(){				   					   
		$records['data'] = $this->inventory_model->get_all_inventory();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = ($row['status'] == 1)? 'checked': '';
			$data[]= array(
				++$i,
				$row['inventory_name'],
				'<input class="tgl_checkbox tgl-ios" 
				data-id="'.$row['inventory_id'].'" 
				id="cb_'.$row['inventory_id'].'"
				type="checkbox"  
				'.$status.'><label for="cb_'.$row['inventory_id'].'"></label>',		
				'<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/inventory/edit/'.$row['inventory_id']).'"> <i class="fa fa-pencil-square-o"></i></a>'
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}
	function change_status()
	{   
		$this->inventory_model->change_status();
	}
	public function add(){
		$this->rbac->check_operation_access(); // check opration permission
		if($this->input->post('submit')){
			$this->form_validation->set_rules('inventory_name', 'Inventory Name', 'trim|required');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/inventory/add'),'refresh');
			}
			else{
				$data = array(
					'inventory_name' => $this->input->post('inventory_name'),
					'status' => $this->input->post('status'),
				);
				$data = $this->security->xss_clean($data);
				
				$result = $this->inventory_model->add_inventory($data);
				if($result){
					$this->session->set_flashdata('success', 'Inventory has been added successfully!');
					redirect(base_url('admin/inventory'));
				}
			}
		}
		else{
			$data['title'] = 'Add Inventory';
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/inventory/add_inventory',$data);
			$this->load->view('admin/includes/_footer');
		}
	}
		public function edit($id = 0){

		$this->rbac->check_operation_access(); // check opration permission

		if($this->input->post('submit')){
			$this->form_validation->set_rules('inventory_name', 'Inventory Name', 'trim|required');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
					$data = array(
						'errors' => validation_errors()
					);
					$this->session->set_flashdata('errors', $data['errors']);
					redirect(base_url('admin/inventory/edit/'.$id),'refresh');
			}
			else{
				$data = array(
					'inventory_name' => $this->input->post('inventory_name'),
					'status' => $this->input->post('status'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->inventory_model->edit_inventory($data, $id);
				if($result){
					$this->session->set_flashdata('success', 'Inventory has been updated successfully!');
					redirect(base_url('admin/inventory'));
				}
			}
		}
		else{
			$data['inventoryData'] = $this->inventory_model->get_inventory_by_id($id);
			$data['title'] = 'Edit Inventory';
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/inventory/inventory_edit', $data);
			$this->load->view('admin/includes/_footer');
		}
	}
}

?>	