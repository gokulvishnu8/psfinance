<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Dashboard extends My_Controller {



	public function __construct(){

		parent::__construct();

		auth_check(); // check login auth

		$this->rbac->check_module_access();

		if($this->uri->segment(3) != '')
		$this->rbac->check_operation_access();

		$this->load->model('admin/dashboard_model', 'dashboard_model');
		$this->load->model('admin/Report_model', 'Report_model');
	}

	//--------------------------------------------------------------------------

	public function index(){

		$data['title'] = 'Dashboard';
		$data['loanlist'] = $this->dashboard_model->get_loan();
		$limit = date('Y-m-d 00:00:00', strtotime('-18 months'));//1.5 yr
		foreach ($data['loanlist'] as $value) {
			if(strtotime($value['updated_on']) < strtotime($limit)) {
				$Loan_id=$value['loan_id'];
			    $data['auction_trigger'] = $this->dashboard_model->auction_trigger($Loan_id);
			}

		}
		$data['customerCount'] =$this->dashboard_model->get_all_users();
		$data['loanCount'] =$this->dashboard_model->get_all_loans();
		$data['inventoryCount'] =$this->dashboard_model->get_all_inventory();
		$data['auctionCount'] =$this->dashboard_model->get_all_loan_auction();
		$data['interestData'] = $this->dashboard_model->get_all_interestInfo();
		$data['loanlistdash'] = $this->dashboard_model->get_loan_dashboard();
		$data['inventory'] = $this->dashboard_model->get_all_inventory_list();
		$data['invoice_detail'] = $this->dashboard_model->get_all_invoices();
		$this->load->view('admin/includes/_header', $data);

    	$this->load->view('admin/dashboard/general');

    	$this->load->view('admin/includes/_footer');

	}

	//--------------------------------------------------------------------------

	public function index_1(){

		$data['all_users'] = $this->dashboard_model->get_all_users();

		$data['active_users'] = $this->dashboard_model->get_active_users();

		$data['deactive_users'] = $this->dashboard_model->get_deactive_users();

		$data['title'] = 'Dashboard';

		$this->load->view('admin/includes/_header', $data);

    	$this->load->view('admin/dashboard/index', $data);

    	$this->load->view('admin/includes/_footer');

	}



	//--------------------------------------------------------------------------

	public function index_2(){

		$data['title'] = 'Dashboard';


		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/index2');

    	$this->load->view('admin/includes/_footer');

	}



	//--------------------------------------------------------------------------

	public function index_3(){

		$data['title'] = 'Dashboard';

		$this->load->view('admin/includes/_header');

    	$this->load->view('admin/dashboard/index3');

    	$this->load->view('admin/includes/_footer');

	}
	public function updateTodayRate()
 {
	 $amt= $this->input->get('amt');
	 $chk=$this->dashboard_model->ifTodaysRateUpdated();
	// if($chk > 0)
	// {
	//	 echo 3;
	 //}
	 //else
	// {
	 $result = $this->dashboard_model->update_interest_rate($amt);
	 if($result)
	 {
		 echo 1;
	 }
	 else
	 {
		echo 2;
	 }
	// }

 }

}
?>
