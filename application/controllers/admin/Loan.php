<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Loan extends MY_Controller {

	public function __construct(){

		parent::__construct();
		auth_check(); // check login auth
		$this->rbac->check_module_access();
		$this->load->model('admin/Loan_model', 'loan_model');
		$this->load->model('admin/User_model', 'user_model');
		$this->load->model('admin/Interest_model', 'interest_model');
		$this->load->model('admin/Invoice_model', 'invoice_model');
		$this->load->model('admin/Report_model', 'Report_model');
		$this->load->helper('db_helper');
	}
	public function index(){
		$data['loanlist'] = $this->loan_model->get_all_loan();
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/loan/loan_list',$data);
		$this->load->view('admin/includes/_footer');
	}
	public function datatable_json(){
		$records['data'] = $this->loan_model->get_all_loan();
		$data = array();

		$i=0;
		//echo '<pre>';print_r($records['data']);exit;
		foreach ($records['data']   as $row)
		{
			if($row['loan_progress']=='auction')
			{
				$loan_stat ='<label style="color:red">Auction</label>';
			}
			else if($row['loan_progress']=='ongoing')
			{
				$loan_stat ='<label style="color:green">Ongoing</label>';
			}
			else if($row['loan_progress']=='closed')
			{
				$loan_stat ='<label style="color:#17a2b8">Closed</label>';
			}
			$status = ($row['loan_status'] == 1)? 'checked': '';
			$progress = ($row['loan_progress'] == 'closed')? 'disabled': '';
			$data[]= array(
				++$i,
				$row['loan_id'],
				$row['firstname']." ".$row['lastname'],
				$row['amount'],
				date_time($row['created_on']),
				$loan_stat,
				//'<input class="tgl_checkbox tgl-ios"
				//data-id="'.$row['loan_id'].'"
				//id="cb_'.$row['loan_id'].'"
				//type="checkbox"
				//'.$status.'><label for="cb_'.$row['loan_id'].'"></label>',
				'<a title="View" class="view btn btn-sm btn-info '.$progress.'" href="'.base_url('admin/loan/view/'.$row['loan_id']).'"> <i class="fa fa-eye"></i></a>'
			);
		}
		$records['data']=$data;
		//echo '';print_r($records['data']);exit;
		echo json_encode($records);
	}
	function change_status()
	{
		$this->loan_model->change_status();
	}
	public function add(){
		$this->rbac->check_operation_access(); // check opration permission
		if($this->input->post('submit')){
			$this->form_validation->set_rules('username', 'Customer Name', 'trim|required');
			$this->form_validation->set_rules('items[]', 'Inventory Item', 'trim|required');
			$this->form_validation->set_rules('grossweights[]', 'Gross Weight', 'trim|required');
			$this->form_validation->set_rules('weights[]', 'Net Weight', 'trim|required');
			$this->form_validation->set_rules('quantites[]', 'Quantity', 'trim|required');
			$this->form_validation->set_rules('prices[]', 'Price', 'trim|required');
			//$this->form_validation->set_rules('remarks[]', 'Remark', 'trim|required');
			//$this->form_validation->set_rules('images[]', 'Image', 'required');
			$this->form_validation->set_rules('given_loan_price', 'Given Loan Amount', 'trim|required');
			$this->form_validation->set_rules('created_at', 'Loan Created Date', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/loan/add'),'refresh');
			}
			else{
				$todays_rate = $this->interest_model->getInterestRate();
				$data = array(
					'customer_id' => $this->input->post('username'),
					'created_at'  => $this->input->post('created_at'),
					'updated_at'  => $this->input->post('created_at'),
					'given_loan_amount'  => $this->input->post('given_loan_price'),
					'loan_status' => 1,
					'rate_for_day' => $todays_rate?(int)$todays_rate[0]['today_gold_rate']:0,
				);
				$data = $this->security->xss_clean($data);
				$result = $this->loan_model->add_loan($data);
				$inventoryData = $this->input->post('items');
				$grossweightData = $this->input->post('grossweights');
				$weightData = $this->input->post('weights');
				$quantityData = $this->input->post('quantites');
				$priceData = $this->input->post('prices');
				$remarkData = $this->input->post('remarks');
				$imgData = $this->input->post('imgData');
				$purities = $this->input->post('purities');
				$data = array();
				// if(!empty($_FILES['images']['name']))
				// {
				// $filesCount = count($_FILES['images']['name']);
				// for($i = 0; $i < $filesCount; $i++){
				// $_FILES['file']['name']        	= $_FILES['images']['name'][$i];
				// $_FILES['file']['type']     	= $_FILES['images']['type'][$i];
				// $_FILES['file']['tmp_name']     = $_FILES['images']['tmp_name'][$i];
				// $_FILES['file']['error']     	= $_FILES['images']['error'][$i];
				// $_FILES['file']['size']     	= $_FILES['images']['size'][$i];
				// $uploadPath                     = './uploads/';
				// $configImg['upload_path'] 	    = $uploadPath;
				// $configImg['allowed_types']     = 'jpg|jpeg|png|gif';
				// $this->load->library('upload', $configImg);
				// $this->upload->initialize($configImg);
				// if($this->upload->do_upload('file')){
				// $fileData = $this->upload->data();
				// $uploadData[$i]['file_name'] 	= $fileData['file_name'];
				// }
				// }
				//     $imageArray =$uploadData;
				// }
				// else
				// {
				// 	$imageArray ='';
				// }

				if($result){
					for ($x = 0; $x < count($inventoryData); $x++) {
			   			$loanInventorydata = array(
							'loan_ID'      => $result,
							'inventory_id' => $inventoryData[$x],
							'gross_weight' => $grossweightData[$x],
							'weight'       => $weightData[$x],
							'quantity'     => $quantityData[$x],
							'price'        => $priceData[$x],
							'purity'       => $purities[$x],
							'remark'       => $remarkData[$x],
							'image'		   => $imgData[$x]
						);
					    $loan_inventory = $this->loan_model->add_loan_inventory($loanInventorydata);
					    $this->loan_model->update_total_amount($this->input->post('given_loan_price'),array_sum($priceData),$result);
					}
					$this->session->set_flashdata('success', 'Loan has been added successfully!');
					//redirect(base_url('admin/loan'));
					redirect(base_url('admin/loan/view/'.$result));
				}
			}
		}
		else{
			$data['title'] = 'Add Loan';
			$data['users'] = $this->loan_model->get_all_users();
			$data['inventoryData'] = $this->loan_model->get_all_inventory();
			$data['interestData'] = $this->loan_model->get_all_interestInfo();
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/loan/add_loan',$data);
			$this->load->view('admin/includes/_footer');
		}
	}
		public function view($id = 0){

		$this->rbac->check_operation_access(); // check opration permission

		if($this->input->post('submit')){
			$this->form_validation->set_rules('inventory_name', 'Inventory Name', 'trim|required');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
					$data = array(
						'errors' => validation_errors()
					);
					$this->session->set_flashdata('errors', $data['errors']);
					redirect(base_url('admin/inventory/edit/'.$id),'refresh');
			}
			else{
				$data = array(
					'inventory_name' => $this->input->post('inventory_name'),
					'status' => $this->input->post('status'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->inventory_model->edit_inventory($data, $id);
				if($result){
					$this->session->set_flashdata('success', 'Inventory has been updated successfully!');
					redirect(base_url('admin/inventory'));
				}
			}
		}
		else{
			$data['loanData'] = $this->loan_model->get_loan_by_id($id);
			$data['loanInventory'] = $this->loan_model->get_loan_inventory_id($id);
			$data['inventoryData'] = $this->loan_model->get_all_inventory();
			$data['users'] = $this->loan_model->get_all_users();
			$data['title'] = 'View Loan';
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/loan/loan_edit', $data);
			$this->load->view('admin/includes/_footer');
		}
	}
	public function loan_payments($id = 0){
		$data['id'] = $id;
		$data['customer_id'] = 0;
		$data['title'] = 'Loan Payments';
		$data['loans'] = $this->loan_model->get_all_ongoing_loan();
		$data['total'] = 0;
		$data['amount'] = 0;
		$data['interest'] = 0;
		$data['days'] = 0;
		$data['default_interest'] = 0;
		if($id != 0){
			$data['loan_details'] = $this->loan_model->get_all_loan_by_id($id);
			$data['user_details'] = $this->user_model->get_user_by_id($data['loan_details']->customer_id);
			$data['loan_inventory_details'] = $this->loan_model->get_loan_inventory_id($id);
			$data['customer_id'] = $data['loan_details']->customer_id;

			$created_date = date("Y-m-d",strtotime($data['loan_details']->created_at));
			$updated_date = date("Y-m-d",strtotime($data['loan_details']->updated_at));
			$today = date("Y-m-d");

			$diff = strtotime($today) - strtotime($updated_date);

			// 1 day = 24 hours
			// 24 * 60 * 60 = 86400 seconds
			$data['days'] = abs(round($diff / 86400));
			$data['amount'] = $data['loan_details']->amount;
			if($data['days'] >= 548){
				$data['interest'] = ($data['amount'] * 27) / 100 / 12 / 30 * intval($data['days']);
			}else if($data['days'] >= 366 && $data['days'] <= 547){
				$data['interest'] = ($data['amount'] * 27) / 100 / 12 / 30 * intval($data['days']);
			}else if($data['days'] >= 181 && $data['days'] <= 365){
				$data['interest'] = ($data['amount'] * 24) / 100 / 12 / 30 * intval($data['days']);
			}else if($data['days'] >= 91 && $data['days'] <= 180){
				$data['interest'] = ($data['amount'] * 21) / 100 / 12 / 30 * intval($data['days']);
			}else if($data['days'] >= 1 && $data['days'] <= 90){
				$data['interest'] = ($data['amount'] * 18) / 100 / 12 / 30 * intval($data['days']);
			}else{
				$data['interest'] = 0;
			}
			$data['default_interest'] = ($data['amount'] * 16) / 100 / 12 / 30 * intval($data['days']);
			$data['total'] = $data['amount'] + $data['interest'];
		}
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/loan/loan_payments', $data);
		$this->load->view('admin/includes/_footer');
	}


public function complete_payment(){
	$id = $this->input->post('id');
	$customer_id = $this->input->post('customer_id');
	$amt = $this->input->post('amt');
	$payment_amt = $this->input->post('payment_amt');
	$total_amt = $this->input->post('total_amt');
	$service_charge = $this->input->post('service_charge');
	$total_interest = $this->input->post('total_interest');
	$default_interest = $this->input->post('default_interest');
	$days = $this->input->post('days');
	$balance = $total_amt - $amt;
	$close = false;
	$result = '';
	if($amt == $total_amt){
		$close = true;
	}else{
		$close = false;
	}
	$this->loan_model->complete_payment($id, $balance, $close);
	$data = array(
		'loan_id'    => $id,
		'customer_id'=> $customer_id,
		'amount'     => $payment_amt,
		'created_at' => date('Y-m-d : h:m:s'),
		'status'     => '1',
	);
	// $this->loan_model->add_to_payments($data);
	// $this->loan_model->update_amount_inventory($id, $amt, $total_amt);


	// $data['company_data'] = array(
	// 	'name' => 'PS Finance',
	// 	'address1' => 'Tamil Nadu',
	// 	'address2' => 'Tamil Nadu',
	// 	'email' => 'psfinance@gmail.com',
	// 	'mobile_no' => '23489347238',
	// 	'created_date' => date('Y-m-d h:m:s')
	// );
	// $data = $this->security->xss_clean($data['company_data']);
	// $company_id = $this->invoice_model->add_company($data);
	$company_id = 1;
	if($company_id){
		$items_detail =  array(
				'product_description' => $this->input->post('product_description'),
				'quantity' => $this->input->post('quantity'),
				'weight' => $this->input->post('weight'),
				'price' => $this->input->post('price'),
				'interest' => $this->input->post('interest'),
				'total' => $this->input->post('total'),
			);
		$items_detail = serialize($items_detail);

		$data['invoice_data'] = array(

			// 'admin_id' => $this->session->userdata('admin_id'),
			'loanid' => $id,
			'user_id' => $customer_id,
			'company_id' => $company_id,
			'invoice_no' => 'INV',
			'txn_id' => '',
			'items_detail' => $items_detail,
			'sub_total' => $amt,
			'total_tax' => $total_interest,
			'default_interest' => $default_interest,
			'service_charge' => $service_charge,
			'grand_total' => $amt + (int)$total_interest + (int)$service_charge,
			'currency ' => '₹',
			'payment_method' => 'CASH',
			'payment_status ' => 'Paid',
			// 'client_note ' => $this->input->post('client_note'),
			// 'termsncondition ' => $this->input->post('termsncondition'),
			'due_date' => (int)$days,
			'created_date' => date('Y-m-d'),
			'loanid'=>$id
		);

		$invoice_data = $this->security->xss_clean($data['invoice_data']);

		$result = $this->invoice_model->add_invoice($invoice_data);
	}
	$response = array(
		'status' => true,
		'invoice_id' => (int)$result
	);
	echo json_encode($response);
}
public function payment_report($id = 0){
	$this->rbac->check_operation_access(); // check opration permission

	$data['invoice_detail'] = $this->invoice_model->get_invoice_by_id($id);

	$this->load->view('admin/includes/_header');
	$this->load->view('admin/loan/payment_report_view',$data);
	$this->load->view('admin/includes/_footer');
}
// Download PDF Loan
public function loan_pdf_download($id=0){
	$data['loanInventory'] = $this->Report_model->get_loan_inventory($id);
	$data['loanInfo'] = $this->Report_model->get_loan_by_id($id);
	// echo '<pre>';print_r($data['loanInfo']);exit;
	$username = $data['loanInfo']['customer_id'];
	$data['user'] = $this->Report_model->get_user_by_id($username);
	$data['loan_id'] = $id;
	$this->load->view('admin/loan/loan_pdf_download', $data);
}
}

?>
