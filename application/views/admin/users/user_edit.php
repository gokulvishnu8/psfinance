  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-pencil"></i>
              &nbsp; <?= trans('edit_user') ?> </h3>
          </div>
          <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/users'); ?>" class="btn btn-success"><i class="fa fa-list"></i> <?= trans('users_list') ?></a>
            <a href="<?= base_url('admin/users/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_user') ?></a>
          </div>
        </div>
        <div class="card-body">

           <!-- For Messages -->
            <?php $this->load->view('admin/includes/_messages.php') ?>

            <?php echo form_open(base_url('admin/users/edit/'.$user['id']),'id="form"', 'class="form-horizontal"' )?>
              <div class="form-group">
                <label for="username" class="col-md-2 control-label">Choose Image</label>

                <div class="row" id="profile_image">
                  <div class="col-md-6">
                    <div id="my_camera"></div>
                  </div>
                  <div class="col-md-6" style="text-align: center;">
                    <div style="display: none;" id="results"></div>
                    <!-- <input type="button" style="display: none;margin: 5px;" class="btn btn-info" value="Submit" id="saveImage"> -->
                  </div>
                </div>
                <div class="row">
                <div class="col-md-2">
                  <div class="imgDisplay" style="margin-bottom:8px;"><img width="100%" src="<?= $user['image']; ?>"/></div><i onClick="start_snapshot()" title="Start Webcam" class="fa fa-camera" style="font-size:24px;cursor: pointer;margin-right:10px"></i><input type=button value="Take Snapshot" class="btn btn-warning take" onClick="take_snapshot()" style="margin-bottom:8px;"><input class="form-control" type="hidden" name="imgData">
                  <!-- <input type="text" name="username" class="form-control" id="username" placeholder=""> -->
                </div>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label"><?= trans('firstname') ?></label>

                <div class="col-md-12">
                  <input type="text" name="firstname" value="<?= $user['firstname']; ?>" class="form-control" id="firstname" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <label for="lastname" class="col-md-2 control-label"><?= trans('lastname') ?></label>

                <div class="col-md-12">
                  <input type="text" name="lastname" value="<?= $user['lastname']; ?>" class="form-control" id="lastname" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <label for="email" class="col-md-2 control-label"><?= trans('email') ?></label>

                <div class="col-md-12">
                  <input pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" type="email" name="email" value="<?= $user['email']; ?>" class="form-control" id="email" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="mobile_no" class="col-md-2 control-label"><?= trans('mobile_no') ?></label>

                <div class="col-md-12">
                  <input type="text" name="mobile_no" value="<?= $user['mobile_no']; ?>" class="form-control" id="mobile_no" placeholder="">
                      <label style="display:none;font-size: small !important;color: red !important;font-weight: 400 !important;" id="mobilecount"  for="mobile">Mobile number should be 10 digit</label>
                </div>
              </div>
              <div class="form-group">
                <label for="id_type" class="col-md-2 control-label">Choose ID</label>

                <div class="col-md-12">
                  <select type="text"  name="id_type" class="form-control" id="id_type" placeholder="">
                    <option value="1" <?= $user['id_type'] == 1 ? 'selected' : '' ?>>Aadhar</option>
                    <option value="2" <?= $user['id_type'] == 2 ? 'selected' : '' ?>>Voters ID</option>
                    <option value="3" <?= $user['id_type'] == 3 ? 'selected' : '' ?>>Driving License</option>
                    <option value="4" <?= $user['id_type'] == 4 ? 'selected' : '' ?>>Others</option>
                  </select>
                </div>
              </div>
              <div class="form-group" id="othersiddiv" style="display:<?= $user['id_type'] == 4 ? 'block' : 'none' ?>">
                <label for="id_name" class="col-md-4 control-label">Provide ID Name (If Others)</label>

                <div class="col-md-12">
                  <input type="text" value="<?= $user['id_name']; ?>"  name="id_name" class="form-control" id="id_name" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="aadhar_no" class="col-md-2 control-label">ID Number</label>

                <div class="col-md-12">
                  <input type="text" name="aadhar_no" value="<?= $user['aadhar_no']; ?>" class="form-control" id="aadhar_no" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="pan_no" class="col-md-2 control-label"><?= trans('pan_no') ?></label>

                <div class="col-md-12">
                  <input type="text"  name="pan_no" value="<?= $user['pan_no']; ?>" class="form-control" id="pan_no" placeholder="">
                </div>
              </div>
              <!-- Contact Details -->
              <div class="form-group">
                <label for="contact_details" class="col-md-12 control-label"><?= trans('contact_details') ?></label>

                <div class="col-md-12">
                  <label>
                    <input type="radio" name="contact_details" class="flat-red" value="0" <?php if($user['contact_details'] == 0){echo 'checked';} ?>>
                  </label>
                  <label>
                    No
                  </label>
                </div>
                <div class="col-md-12">
                  <label>
                    <input type="radio" name="contact_details" value="1" class="flat-red" <?php if($user['contact_details'] == 1){echo 'checked';} ?>>
                  </label>
                  <label>
                    Yes
                  </label>
                </div>
                <div class="col-md-12" id="contact_details_div" style="display:<?php if($user['contact_details'] == 0){echo 'none';}else{ echo 'block'; }?>">
                  <fieldset>
                    <legend>Contact Details</legend>
                    <div class="row">
                    <div class="col-md-6">
                      <label for="cname" class="col-md-12 control-label">Contact Name</label>
                      <input type="text" id="cname" name="cname" value="<?= $user['cname']; ?>" class="col-md-12 form-control">
                    </div>
                    <div class="col-md-6">
                      <label for="cno" class="col-md-12 control-label">Contact Number</label>
                      <input type="number" id="cno" name="cno" value="<?= $user['cno']; ?>" class="col-md-12 form-control">
                    </div>
                    </div>
                  </fieldset>
                </div>
              </div>
              <div class="form-group">
                <label for="nominee_details" class="col-md-12 control-label"><?= trans('nominee_details') ?></label>

                <div class="col-md-12">
                  <label>
                    <input type="radio" name="nominee_details" class="flat-red" value="0" <?php if($user['nominee_details'] == 0){echo 'checked';} ?>>
                  </label>
                  <label>
                    No
                  </label>
                </div>
                <div class="col-md-12">
                  <label>
                    <input type="radio" name="nominee_details" value="1" class="flat-red" <?php if($user['nominee_details'] == 1){echo 'checked';} ?>>
                  </label>
                  <label>
                    Yes
                  </label>
                </div>
                <div class="col-md-12" id="nominee_details_div" style="display:<?php if($user['nominee_details'] == 0){echo 'none';}else{ echo 'block'; }?>">
                  <fieldset>
                    <legend>Nominee Details</legend>
                    <div class="row">
                    <div class="col-md-6">
                      <label for="nom_name" class="col-md-12 control-label">Nominee Name</label>
                      <input type="text" id="nom_name" name="nom_name" value="<?= $user['nom_name']; ?>" class="col-md-12 form-control">
                    </div>
                    <div class="col-md-6">
                      <label for="nom_rel" class="col-md-12 control-label">Relationship</label>
                      <input type="text" id="nom_rel" name="nom_rel" value="<?= $user['nom_rel']; ?>" class="col-md-12 form-control">
                    </div>
                    </div>
                  </fieldset>
                </div>
              </div>
              <!-- Banking Details -->
              <div class="form-group">
                <label for="banking_details" class="col-md-12 control-label"><?= trans('banking_details') ?></label>

                <div class="col-md-12">
                  <label>
                    <input type="radio" name="banking_details" class="flat-red" value="0" <?php if($user['banking_details'] == 0){echo 'checked';} ?>>
                  </label>
                  <label>
                    No
                  </label>
                </div>
                <div class="col-md-12">
                  <label>
                    <input type="radio" name="banking_details" value="1" class="flat-red" <?php if($user['banking_details'] == 1){echo 'checked';} ?>>
                  </label>
                  <label>
                    Yes
                  </label>
                </div>
                <div class="col-md-12" id="banking_details_div" style="display:<?php if($user['banking_details'] == 0){echo 'none';}else{ echo 'block'; }?>">
                  <fieldset>
                    <legend>Bank Details</legend>
                    <div class="row">
                    <div class="col-md-6">
                      <label for="bankname" class="col-md-12 control-label">Bank Name</label>
                      <input type="text" value="<?= $user['bankname']; ?>" id="bankname" name="bankname" class="col-md-12 form-control">
                    </div>
                    <div class="col-md-6">
                      <label for="acc_no" class="col-md-12 control-label">Account Number</label>
                      <input type="text" value="<?= $user['acc_no']; ?>" id="acc_no" name="acc_no" class="col-md-12 form-control">
                    </div>
                    <div class="col-md-6">
                      <label for="ifsc" class="col-md-12 control-label">IFSC Code</label>
                      <input type="text" value="<?= $user['ifsc']; ?>" id="ifsc" name="ifsc" class="col-md-12 form-control">
                    </div>
                    <div class="col-md-6">
                      <label for="branch" class="col-md-12 control-label">Branch</label>
                      <input type="text" value="<?= $user['branch']; ?>" id="branch" name="branch" class="col-md-12 form-control">
                    </div>
                    </div>
                  </fieldset>
                </div>
              </div>
              <!-- <div class="form-group">
                <label for="password" class="col-md-2 control-label"><?= trans('password') ?></label>

                <div class="col-md-12">
                  <input type="password" name="password" class="form-control" id="password" placeholder="">
                </div>
              </div> -->
              <div class="form-group">
                <label for="address" class="col-md-2 control-label"><?= trans('address') ?></label>

                <div class="col-md-12">
                  <textarea name="address" class="form-control" id="address" placeholder="" rows="4"><?= $user['address']; ?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="role" class="col-md-2 control-label"><?= trans('status') ?></label>

                <div class="col-md-12">
                  <select name="status" class="form-control">
                    <option value=""><?= trans('select_status') ?></option>
                    <option value="1" <?= ($user['is_active'] == 1)?'selected': '' ?> >Active</option>
                    <option value="0" <?= ($user['is_active'] == 0)?'selected': '' ?>>Deactive</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <input type="submit" name="submit" value="<?= trans('update_user') ?>" class="btn btn-primary pull-left">
                </div>
              </div>
            <?php echo form_close(); ?>
        </div>
          <!-- /.box-body -->
      </div>
    </section>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
     <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
 <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
  <script>
    $(document).ready(function(){
      $('input[type=radio][name="contact_details"]').change(function(){
        if(this.value == 1) {
          $('#contact_details_div').show();
        }else{
          $('#contact_details_div').hide();
        }
      });
      $('input[type=radio][name="nominee_details"]').change(function(){
        if(this.value == 1) {
          $('#nominee_details_div').show();
        }else{
          $('#nominee_details_div').hide();
        }
      });
      $('input[type=radio][name="banking_details"]').change(function(){
        if(this.value == 1) {
          $('#banking_details_div').show();
        }else{
          $('#banking_details_div').hide();
        }
      });
      $('#id_type').change(function () { 
        if($(this).val() == 4){
          $('#othersiddiv').show();
        }else{
          $('#othersiddiv').hide();
        }
      });
    });
  </script>
  <script type="text/javascript">
  $.validator.addMethod("alpha", function(value, element) {
      return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
      // --                                    or leave a space here ^^
  });
  $.validator.addMethod("varcharRegex", function(value, element) {
          return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
      }, "Please enter character and number");

   $.validator.addMethod("emailExt", function(value, element, param) {
      return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
  },'Please enter a vaild email address');
  $.validator.addMethod("varchardashRegex", function(value, element) {
          return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
      });
  $.validator.addMethod("addressRegex", function(value, element) {
    return this.optional(element) || /^[a-z0-9\-\/\,\s]+$/i.test(value);
  },"Special characters not allowed");
  $.validator.addMethod("mobileValidation",function(value, element) {
    return !/^\d{8}$|^\d{10}$/.test(value) ? false : true;
},"Mobile number is invalid");
  $("#form").validate({

      onfocusout: false,
      invalidHandler: function(form, validator) {
          var errors = validator.numberOfInvalids();
          if (errors) {
              validator.errorList[0].element.focus();
          }
      } ,
    error: function(label) {
       $(this).addClass("error");
     },
     rules: {
           firstname: {
              required: true,
              alpha:true
          },
           lastname: {
              required: true,
              alpha:true
          },
          email: {
          emailExt: true,
          required: true
        },
        mobile_no: {
          required: true,
          maxlength: 10,
          mobileValidation:true
        },
         id_type: {
          required: true,
        },
         id_name: {
          required: true,
        },
         aadhar_no: {
          required: true,
        },
        address: {
          required: true,
        },
        cname: {
              required: {
                  depends: function(element) {
                      return $('input[name="contact_details"]:checked').val()==1
                  }
              },
              alpha:true
          },
        cno: {
              required: {
                  depends: function(element) {
                      return $('input[name="contact_details"]:checked').val()==1
                  }
              }
          },
        nom_name: {
              required: {
                  depends: function(element) {
                      return $('input[name="nominee_details"]:checked').val()==1
                  }
              },
              alpha:true
          },
        nom_rel: {
              required: {
                  depends: function(element) {
                      return $('input[name="nominee_details"]:checked').val()==1
                  }
              },
              alpha:true
          },
         bankname: {
              required: {
                  depends: function(element) {
                      return $('input[name="banking_details"]:checked').val()==1
                  }
              },
              varchardashRegex:true
          },
        acc_no: {
              required: {
                  depends: function(element) {
                      return $('input[name="banking_details"]:checked').val()==1
                  }
              },
              digits: true
          },
        ifsc: {
              required: {
                  depends: function(element) {
                      return $('input[name="banking_details"]:checked').val()==1
                  }
              },
              varcharRegex:true
          },
        branch: {
              required: {
                  depends: function(element) {
                      return $('input[name="banking_details"]:checked').val()==1
                  }
              },
              alpha:true
          },
          address:
          {
            addressRegex:true
          }
    },
    messages: {

    firstname: {
              required:"Please enter First Name",
                alpha:"First Name must contain only letters"
          } ,
    lastname: {
          required: "Please enter Last Name",
          alpha:"Last Name must contain only letters"
        },
    email: {
          required: "Please enter Email address",
          emailExt:"Please enter a vaild email address"
      },
    mobile_no: {
          required: "Please enter Mobile Number",
          maxlength: "Please enter 10 digits"
        },
    id_type: {
        required: "Please choose the ID type",
      },
    id_name: {
        required: "Please enter the name of ID",
      },
    aadhar_no: {
          required: "Please enter ID number",
        },
    address: {
          required: "Please enter Address",
        },
    cname: {
          required: "Please enter Contact Name",
          alpha:"Contact Name must contain only letters"
        },
    cno: {
          required: "Please enter Contact Number",
        },
    nom_name: {
          required: "Please enter Nominee Name",
          alpha:"Nominee Name must contain only letters"
        },
    nom_rel: {
          required: "Please enter Relationship",
          alpha:"Nominee Name must contain only letters"
        },
    bankname: {
          required: "Please enter Bank Name",
          varchardashRegex:"Bank Name must contain only letters, numbers, or dashes."
        },
    acc_no: {
          required: "Please enter Account Number",
          digits: "Account number must contain only numbers"
        },
    ifsc: {
          required: "Please enter IFSC Code",
          varcharRegex:"IFSC Code must contain only letters,numbers"
        },
    branch: {
          required: "Please enter Branch",
          alpha:"Branch must contain only letters"
        },
      }

  });

  </script>
<script language="JavaScript">
    Webcam.set({
        width: 490,
        height: 390,
        image_format: 'jpeg',
        jpeg_quality: 90
    });

   // Webcam.attach( '#my_camera' );

    //function take_snapshot() {
        //Webcam.snap( function(data_uri) {
          //  $(".image-tag").val(data_uri);
          // document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
            //$(this).siblings('.sampleImg').val(4);
            //console.log($(this).siblings('.sampleImg').children())
            //$(this).siblings('input').val(2);
            //document.getElementsByClassName('sampleImg').innerHTML = '<img  width="50" height="40" src="'+data_uri+'"/>';
            //$('#saveImage').show();
       // } );
    //}
    $(document).on('click', '.take', function (e) {
          Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
           document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';

            //document.getElementsByClassName('sampleImg').innerHTML = '<img  width="50" height="40" src="'+data_uri+'"/>';
            //$('#saveImage').show();
        } );
          var imgUrl =$('#results').children('img').attr('src');
          $(this).siblings('input').val(imgUrl);
          $(this).siblings('.imgDisplay').html('<img  width="100%" src="'+imgUrl+'"/>');
          $('#profile_image').hide();
          Webcam.reset();
          //document.getElementsByClassName('imgDisplay').innerHTML = '<img  width="50" height="40" src="'+imgUrl+'"/>';
    } );
    function start_snapshot() {
        $('#profile_image').show();
        Webcam.attach( '#my_camera' );
        $('#results').show();
    }

</script>