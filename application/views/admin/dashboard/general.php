  <!-- Content Wrapper. Contains page content -->
  <style>

  </style>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?= trans('dashboard') ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"><?= trans('home') ?></a></li>
              <li class="breadcrumb-item active"><?= trans('dashboard') ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Customers</span>
                <span class="info-box-number">
                <?php echo !empty($customerCount)? $customerCount : ''?>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-money"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Loans</span>
                <span class="info-box-number"><?php echo !empty($loanCount)? $loanCount : ''?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fa fa-list-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Inventory</span>
                <span class="info-box-number"><?php echo !empty($inventoryCount)? $inventoryCount : ''?></span></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-balance-scale"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Auction</span>
                <span class="info-box-number"><?php echo !empty($auctionCount)? $auctionCount : ''?></span></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
          <!-- ############ -->
          <div class="col-md-12">
            <div class="info-box mb-3 bg-warning">
              <span class="info-box-icon"><i class="fa fa-money"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Today's Gold Rate[/gram]</span>
                <span class="info-box-number"><span class="info-box-number" id="spanval" style="font-size:30px"><?php echo !empty($interestData['today_gold_rate'])? $interestData['today_gold_rate'].' &#x20b9' : ''?></span></span></span>
              </div>
              <a title="Add today's Gold Rate [&#x20b9/g]" href="#"><button data-toggle="modal" data-target="#modal-default" type="button" class="btn bg-gradient-success" style="background: #dd1313;height: 36px;"><i class="fa fa-plus" aria-hidden="true" style="color: white;"></i></button></a>
            </div>
          </div>
          <!-- ########## -->
          <div class="col-md-6">
            <div class="card">
                <div class="card-header border-transparent bg-dark">
                  <h3 class="card-title">Latest Loans</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse">
                      <i class="fa fa-minus" style="color: white;"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove">
                      <i class="fa fa-times" style="color: white;"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table m-0">
                      <thead>
                      <tr>
                        <th>Loan ID</th>
                        <th>Customer Name</th>
                        <th>Loan Amount</th>
                        <th>Created at</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php if(!empty($loanlistdash)) {?>
                        <?php foreach ($loanlistdash as $key => $value) { ?>
                      <tr>
                        <td><a href="<?= base_url('admin/loan/view/'.$value['loan_id']); ?>"><?php echo $value['loan_id']; ?></a></td>
                        <td><?php echo $value['firstname']." ".$value['lastname']; ?></td>
                        <td><?php echo $value['amount']?></td>
                        <td><?= date_time($value['created_on']); ?></td>
                      </tr>
                    <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="card-footer clearfix">
                  <a href="<?= base_url('admin/loan/add'); ?>" class="btn btn-sm btn-info float-left">Add Loan</a>
                  <a href="<?= base_url('admin/loan'); ?>" class="btn btn-sm btn-secondary float-right">Loan List</a>
                </div>
              </div>
          </div>
          <!-- ########## -->
          <div class="col-md-6">
            <div class="card">
                <div class="card-header border-transparent bg-dark">
                  <h3 class="card-title" style="color: white;">Latest Invoice</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse">
                      <i class="fa fa-minus" style="color: white;"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove">
                      <i class="fa fa-times" style="color: white;"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table m-0">
                      <thead>
                      <tr>
                        <th>Invoice ID</th>
                        <th>Customer Name</th>
                        <th>Amount</th>
                        <th>Payment Date</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php if(!empty($invoice_detail)) {?>
                        <?php foreach ($invoice_detail as $key => $value) { ?>
                      <tr>
                        <td><a target="_blank" href="<?= base_url('admin/invoices/view/'.$value['id']); ?>"><?= $value['invoice_no'].$value['id']; ?></a></td>
                        <td><?php echo $value['firstname']." ".$value['lastname']; ?></td>
                        <td><?= $value['currency'] .' '. $value['grand_total']; ?></td>
                        <td><?= date_time($value['created_date']); ?></td>
                      </tr>
                    <?php } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="card-footer clearfix">
                  <a href="<?= base_url('admin/invoices'); ?>" class="btn btn-sm btn-secondary float-right">Invoice List</a>
                </div>
            </div>
          </div>
          <!-- ########## -->
          <!-- ########## -->
          <div class="col-md-6"></div>
          <!-- Left col -->
            <div class="col-md-6">


          <!-- /.col -->


            <!-- /.info-box -->
            <!-- <div class="info-box mb-3 bg-success">
              <span class="info-box-icon"><i class="fa fa-heart-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Mentions</span>
                <span class="info-box-number">92,050</span>
              </div>
            </div>
            <div class="info-box mb-3 bg-danger">
              <span class="info-box-icon"><i class="fa fa-cloud-download"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Downloads</span>
                <span class="info-box-number">114,381</span>
              </div>
            </div>
            <div class="info-box mb-3 bg-info">
              <span class="info-box-icon"><i class="fa fa-comment-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Direct Messages</span>
                <span class="info-box-number">163,921</span>
              </div>
            </div> -->
            <!-- /.info-box -->

            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h4 class="modal-title">Gold Rate[/gram]</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group row">
              <label for="inventory_name" class="col-md-5 control-label">Enter Today's Rate[&#x20b9]</label>
            <div class="col-md-6">
              <input id="todayRate" type="text" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="submitRate">Submit</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <!-- PAGE PLUGINS -->
<!-- SparkLine -->
<script src="<?= base_url() ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jVectorMap -->
<script src="<?= base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?= base_url() ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.2 -->
<script src="<?= base_url() ?>assets/plugins/chartjs-old/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->
<script src="<?= base_url() ?>assets/dist/js/pages/dashboard2.js"></script>
<script>
$('#submitRate').click(function (e) {
    e.preventDefault();
    var Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 5000
    });
    var amt = $('#todayRate').val();
    if(amt >0)
    {
      $.ajax({
      url:"dashboard/updateTodayRate",
      method:"GET",
      data:{amt:amt},
      success:function(data)
      {
        if(data==1)
        {
          $('#modal-default').modal('hide');
          $('#todayRate').val('');
          $('#spanval').html(amt+' &#x20b9');
		  Toast.fire({
            icon: 'success',
            title: 'Todays Gold Rate has been updated successfully'
          })
        }
        else if(data==2){
          $('#modal-default').modal('hide');
          $('#todayRate').val('');
		  Toast.fire({
            icon: 'error',
            title: 'Something went wrong'
          })
        }
      }
    });
    }
});
</script>
