<?php
require_once  $_SERVER['DOCUMENT_ROOT'] . '/psfinance/vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf(['margin_left' => 25,'margin_right' => 25]);
$html = '
	<section>
		<table border="" style="width: 100%">
			<tbody>
				<tr>
					<td width="10%" align="center" style="text-align:center"><img class="logo" src="'.base_url().'/assets/img/logo.jpg" width="150px"/></td>
					<td style="text-align:center">
						<h3>'.ucfirst($invoice_detail['company_name']).'</h3>
						<p> '.$invoice_detail['company_address1'].' </p>
						<p> '.$invoice_detail['company_email'].' </p>
						<p> '.$invoice_detail['company_mobile_no'].' </p>
						<h3> INVOICE </h3>
					</td>
				</tr>
			</tbody>
		</table>
		<table border="" style="width: 100%">
			<tbody>
				<tr>
					<td>
						<h4>RELEASE DATE : '.date_time($invoice_detail['created_date']).'</h4>
						<h4>RELEASE ID : '.$invoice_detail['loanid'].'</h4>
					 </td>
				</tr>
			</tbody>
		</table>

		<table class="invoice" border="" style="width: 100%">
			<tbody>
				<tr >
					<th style="margin-right:1mm">
						<p>Billing To</p>
					</th>
				</tr>
				<tr>		
					<td>
						<p><strong> '.ucwords($invoice_detail['firstname'].' '.$invoice_detail['lastname']).' </strong></p>
						<p> '.$invoice_detail['client_address'].' </p>
						<p> '.$invoice_detail['client_email'].' </p>
						<p> '.$invoice_detail['client_mobile_no'].'  </p>
				   	</td>
				</tr>	
			</tbody>
		</table>

		<table align="right" class="calculation bpmTopic" width="90%">
	        <tbody>
				<tr>
					<th>Total Amount Paid ( Amount + Interest + Charges ) :</th>
					<td style="text-align:right;font-weight:900px;">'.$invoice_detail['currency'].' '.$invoice_detail['grand_total'].'</td>
				</tr>
	        </tbody>
	    </table>


	    



	</section>	
';

//==============================================================

$filename = $invoice_detail['invoice_no'].$invoice_detail['id'];

// $mpdf=new mPDF('c','A4','','',25,25,25,25,16,13); 
$mpdf->showImageErrors = true;
$mpdf->curlAllowUnsafeSslRequests = true;

$mpdf->SetDisplayMode('fullpage');
$mpdf->debug = true;
$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents(base_url('assets/dist/css/mpdfstyletables.css'));
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->SetJS('this.print();');
$mpdf->WriteHTML($html,2);
ob_clean();
$mpdf->Output();
// $mpdf->Output($filename.'.pdf','D');
exit;

?>