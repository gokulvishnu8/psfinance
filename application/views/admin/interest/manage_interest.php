<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="card card-default">
      <div class="card-header">
        <div class="d-inline-block">
            <h3 class="card-title">
           <?php echo $title; ?> </h3>
        </div>
       <!--  <div class="d-inline-block float-right">
          <a href="<?= base_url('admin/users'); ?>" class="btn btn-success"><i class="fa fa-list"></i>  <?= trans('users_list') ?></a>
        </div> -->
      </div>
      <div class="card-body">

         <!-- For Messages -->
          <?php $this->load->view('admin/includes/_messages.php') ?>

          <?php echo form_open(base_url('admin/interest/update_interest_rate'),'id="form"', 'class="form-horizontal"');  ?>

           <div class="form-group">
              <label for="actual_interest_rate" class="col-md-2 control-label">Gold Rate[/gram]&#x20b9</label>

              <div class="col-md-3">
                <input  type="text" name="today_gold_rate" value="<?php echo $interestData[0]['today_gold_rate']?>" class="form-control" id="today_gold_rate" placeholder="">
              </div>
            </div>

            <div class="form-group">
              <label for="actual_interest_rate" class="col-md-2 control-label">Actual Interest Rate</label>

              <div class="col-md-3 input-group">
                <input type="number" name="actual_interest_rate" value="<?php echo $interestData[0]['actual_interest_rate']?>" class="form-control" id="actual_interest_rate" placeholder="" readonly>
                <div class="input-group-prepend">
                  <span class="input-group-text">%</span>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="0_3" class="col-md-2 control-label">[0-3] Month</label>

              <div class="col-md-3 input-group">
                <input value="<?php echo $interestData[0]['0_3']?>" type="number" name="0_3" class="form-control" id="0_3" placeholder="" readonly>
                 <div class="input-group-prepend">
                  <span class="input-group-text">%</span>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="3_6" class="col-md-2 control-label">[3-6] Month</label>

              <div class="col-md-3 input-group">
                <input value="<?php echo $interestData[0]['3_6']?>" type="number" name="3_6" class="form-control" id="3_6" placeholder="" readonly>
                 <div class="input-group-prepend">
                  <span class="input-group-text">%</span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="6_12" class="col-md-2 control-label">[6-12] Month</label>

              <div class="col-md-3 input-group">
                <input value="<?php echo $interestData[0]['6_12']?>" type="number" name="6_12" class="form-control" id="6_12" placeholder="" readonly>
                 <div class="input-group-prepend">
                  <span class="input-group-text">%</span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="12_18" class="col-md-2 control-label">[12-18] Month</label>

              <div class="col-md-3 input-group">
                <input value="<?php echo $interestData[0]['12_18']?>" type="number" name="12_18" class="form-control" id="12_18" placeholder="" readonly>
                 <div class="input-group-prepend">
                  <span class="input-group-text">%</span>
                </div>
              </div>
            </div>
            <div class="form-group" style="float:left;">
              <div class="col-md-12">
                <input type="submit" name="submit" value="Update" class="btn btn-primary pull-right">
              </div>
            </div>
          <?php echo form_close( ); ?>
      </div>
        <!-- /.box-body -->
    </div>
  </section>
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script type="text/javascript">
$.validator.addMethod("alpha", function(value, element) {
  return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
  // --                                    or leave a space here ^^
});
$.validator.addMethod("varcharRegex", function(value, element) {
      return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
  }, "Please enter character and number");

$.validator.addMethod("emailExt", function(value, element, param) {
  return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
},'Please enter a vaild email address');

$.validator.addMethod("varchardashRegex", function(value, element) {
      return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
  }, "Inventory Name must contain only letters, numbers, or dashes.");
  jQuery.validator.addMethod("dollarsscents", function (value, element) {
      return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value);
  }, "Enter valid Gold Rate or Rate with 2 decimal places");
$("#form").validate({

  onfocusout: false,
  invalidHandler: function(form, validator) {
      var errors = validator.numberOfInvalids();
      if (errors) {
          validator.errorList[0].element.focus();
      }
  } ,
error: function(label) {
   $(this).addClass("error");
 },
 rules: {
       today_gold_rate: {
          required: true,
          dollarsscents:true
      },
},
messages: {

today_gold_rate: {
          required:"Please enter Gold Rate[/gram]",
      } ,

  },


});

</script>
