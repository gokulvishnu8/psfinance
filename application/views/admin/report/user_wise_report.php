  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-plus"></i>
              <?php echo $title; ?> </h3>
          </div>

        </div>

<!-- <a href="<?= base_url('admin/report/user_report'); ?>" class="btn btn-success"><i class="fa fa-list"></i>ggggggggggg</a> -->
        <div class="card-body" id="pdf">

           <!-- For Messages -->
            <?php $this->load->view('admin/includes/_messages.php') ?>

            <?php echo form_open(base_url('admin/report/generate_pdf'),'id="form"', 'class="form-horizontal"' )?> 

             <div class="form-group">
                <label for="inventory_name" class="col-md-2 control-label">Customer Name</label>

                <div class="col-md-3">
                  <select name="username" id="username" class="form-control select2" style="width: 100%;">
                    <option value="">Please select</option>
                    <?php foreach ($users as $key => $value): ?>
                       <option value="<?= $value['id']; ?>"><?= $value['firstname']; ?> <?= $value['lastname']; ?></option>
                    <?php endforeach ?>

                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inventory_name" class="col-md-2 control-label">Loan</label>
                <div class="col-md-3">
                  <select name="loan_id" id="loan_id" class="form-control" style="width: 100%;">
                    <option value="">Please select</option>

                  </select>
                </div>
              </div>
              <div class="form-group" style="float:left;">
                <div class="col-md-12">
                  <input type="submit" name="submit" value="Generate PDF" class="btn btn-primary pull-right">
                </div>
              </div>
            <?php echo form_close(); ?>
        </div>
          <!-- /.box-body -->
      </div>
    </section>
  </div>
<script>
$(document).ready(function(){
  var i=1;
  var inventory = <?php echo json_encode($inventoryData); ?>;
  $('#add').click(function(){
    i++;
var first='<tr id="row'+i+'"><td><select name="items[]" class="form-control" id="items"><option value="">Select</option>';
var getOption ='';
for (let i = 0; i < inventory.length; i++) {
 getOption += '<option value="'+inventory[i].inventory_id+'">'+inventory[i].inventory_name+'</option>';
}
var option =getOption;
var last='</select></td><td class="wghroot"><input type="text" name="weights[]" placeholder=" " class="form-control name_list wghtcls" /></td><td class="qtyroot"><input type="text" name="quantites[]" placeholder=" " class="form-control name_list qtycls" /></td><td class="priceroot"><input readonly type="text" name="prices[]" placeholder=" " class="form-control name_list" /></td><td><textarea name="remarks[]" placeholder=" " class="form-control name_list"></textarea></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class="fa fa-close"></i></button></td></tr>';

    $('#dynamic_field').append(first+option+last);
  });

  $(document).on('click', '.btn_remove', function(){
    var button_id = $(this).attr("id");
    $('#row'+button_id+'').remove();
  });

  $('#submit').click(function(){
    $.ajax({
      url:"name.php",
      method:"POST",
      data:$('#add_name').serialize(),
      success:function(data)
      {
        //alert(data);
        $('#add_name')[0].reset();
      }
    });
  });

});
</script>
<script type="text/javascript">
$(document).on('keyup', '.qtycls', function (e) {
    var weight     = $(this).parent().siblings('.wghroot').children().val();
    var quantity   = $(this).val();
    var gram_price = '4700';
    var net_price  =  quantity * gram_price * weight;
    $(this).parent().siblings('.priceroot').children().val(net_price);
});
</script>
<script type="text/javascript">
$(document).on('keyup', '.wghtcls', function (e) {
    $(this).parent().siblings('.priceroot').children().val(" ");
    $(this).parent().siblings('.qtyroot').children().val(" ");
});
</script>
<script>
var doc = new jsPDF();

 function saveDiv(divId, title) {
 doc.fromHTML(`<html><head><title>${title}</title></head><body>` + document.getElementById(divId).innerHTML + `</body></html>`);
 doc.save('div.pdf');
}

function printDiv(divId,
  title) {

  let mywindow = window.open('', 'PRINT', 'height=650,width=900,top=100,left=150');

  mywindow.document.write(`<html><head><title>${title}</title>`);
  mywindow.document.write('</head><body >');
  mywindow.document.write(document.getElementById(divId).innerHTML);
  mywindow.document.write('</body></html>');

  mywindow.document.close(); // necessary for IE >= 10
  mywindow.focus(); // necessary for IE >= 10*/

  mywindow.print();
  mywindow.close();

  return true;
}

</script>
<script>
  $("#username").change(function(){
    var userid= $(this).val();
      $.ajax({
      url:"getUserLoan",
      dataType: "html",
      method:"GET",
      data:{userid:userid},
      success:function(data)
      {
        var showData = '';
        showData += "<option value=''>Please select</option>";
        for(var i = 0; i < JSON.parse(data).length; i++)
          {
             showData += '<option value="'+JSON.parse(data)[i].loan_id+'">Loan Id # '+JSON.parse(data)[i].loan_id+'</option>';
          }
          $('#loan_id').html(showData) ;
      }
    });
  });
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script type="text/javascript">
$.validator.addMethod("alpha", function(value, element) {
  return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
  // --                                    or leave a space here ^^
});
$.validator.addMethod("varcharRegex", function(value, element) {
      return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
  }, "Please enter character and number");

$.validator.addMethod("emailExt", function(value, element, param) {
  return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
},'Please enter a vaild email address');

$.validator.addMethod("varchardashRegex", function(value, element) {
      return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
  }, "Inventory Name must contain only letters, numbers, or dashes.");

$("#form").validate({

  onfocusout: false,
  invalidHandler: function(form, validator) {
      var errors = validator.numberOfInvalids();
      if (errors) {
          validator.errorList[0].element.focus();
      }
  } ,
error: function(label) {
   $(this).addClass("error");
 },
 rules: {
       username: {
          required: true,
      },
      loan_id: {
         required: true,
     },
},
messages: {

username: {
          required:"Please select Customer Name",
      } ,
loan_id: {
          required:"Please select Loan",
} ,
  }

});

</script>
