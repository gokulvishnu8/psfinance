<?php
$html = '
  <section>
    <table border="" style="width: 100%">
      <tbody>
        <tr>
          <td width="100%"><h3>User Loan Report</h3></td>
        </tr>
        <tr>
          <td width="100%"><strong>Loan ID #'. $loan_id.'</strong></td>
        </tr>
      </tbody>
    </table>

    <table class="invoice" border="" style="width: 100%">
      <tbody>
        <tr >
          <th style="margin-right:1mm">
            <p>User Info</p>
          </th>
          <th>
            <p></p>
          </th>
        </tr>
        <tr>    
          <td>
            <p><strong>Name : </strong>'.ucwords($user['firstname']." ".$user['lastname']).'</p>
            <p><strong>Address : </strong>'.$user['address'].' </p>
            <p><strong>Email : </strong>'.$user['email'].' </p>
            <p><strong>Mobile : </strong>'.$user['mobile_no'].'  </p>
            <p><strong>Nominee Name :'.$user['nom_name'].' </strong></p>
            <p><strong>Nominee Relation :'.$user['nom_rel'].' </strong></p>
            </td>
            <td>
            <p><strong>PAN Number : </strong>'.$user['pan_no'].' </p>
            <p><strong>Aadhaar Number : </strong>'.$user['aadhar_no'].' </p>
            </td>
        </tr> 
      </tbody>
    </table>

    <strong>Inventory Details</strong>
    <table class="invoice" border="" style="width: 100%">
      <thead>
        <tr class="">
          <th>Inventory Name</th>
            <th>Quantity</th>
            <th>Weight(g)</th>
            <th>Amount(INR)</th>
        </tr>
      </thead>
      <tbody>';
      $items_detail = $loanInventory; 
      $count = count($items_detail); 
      for($i=0; $i<$count; $i++):

      $html .= '
        <tr class="oddrow">
          <td> '.$items_detail[$i]['inventory_name'].' </td>
          <td> '.$items_detail[$i]['quantity'].' </td>
          <td> '.$items_detail[$i]['weight'].' </td>
          <td> '.$items_detail[$i]['price'].' </td>
        </tr>';

      endfor; 


      $html .= '  
      
      </tbody>
    </table>
    <table class="" width="50%">
        <tbody>
          <tr>
            <th>Total Amount: '.$loanInfo['amount'].' INR <span class="inr-sign"></span></th>
          </tr>
        </tbody>
      </table>








  </section>  
';

//==============================================================
date_default_timezone_set("Asia/Kolkata"); 
$filename = '#'.$loan_id.'_'.date('d-m-y h:i:s');
require_once  $_SERVER['DOCUMENT_ROOT'] . '/psfinance/vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf(['margin_left' => 25,'margin_right' => 25]);
// $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');
$mpdf->debug = true;
$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents(base_url('assets/dist/css/mpdfstyletables.css'));
$mpdf->WriteHTML($stylesheet,1);  // The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

ob_clean();
$mpdf->Output($filename.'.pdf','D');
exit;

?>