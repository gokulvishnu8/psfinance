<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css">
<style type="text/css">
button.dt-button, div.dt-button, a.dt-button {
    background-image: linear-gradient(to bottom, #4cae4c 100%, #4cae4c 100%) !important;
    color:white;
    border-color: #4cae4c;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- For Messages -->
    <?php $this->load->view('admin/includes/_messages.php') ?>
    <div class="card">
      <div class="card-header">
        <div class="d-inline-block">
          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp;Closed Loan List</h3>
        </div>
        <div class="d-inline-block float-right">
          <?php if($this->rbac->check_operation_permission('add')): ?>
            <a href="<?= base_url('admin/users/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_user') ?></a>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body table-responsive">
        <table id="naa_datatable" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>
              <th>Sl no</th>
              <th>Loan Id</th>
              <th>Customer Name</th>
              <th>Loan Amount[&#x20B9]</th>
              <th>Total Interest[&#x20B9]</th>
              <th>Closing Amount[&#x20B9]</th>
              <th>Closed on</th>


            </tr>
          </thead>
               <tbody>
           <?php //echo '<pre>';print_r($loanlist);exit;?>
            <?php foreach($loanlist as $key => $data): ?>

            <tr>
              <td><?=  $key +1; ?></td>
              <td><?= $data['loan_id']; ?></td>
              <td><?= $data['firstname']." ".$data['lastname']; ?></td>
              <td><?= number_format($data['loan_amount'],2); ?></td>
              <td><?= number_format($data['total_default_interest'],2); ?></td>
              <td><?= number_format($data['loan_amount'] + $data['total_default_interest'],2); ?></td>
              <td><?= date_time($data['closed_on']); ?></td>



            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script>
  $(document).ready(function() {
        var table = $('#naa_datatable').DataTable( {
            stateSave: true,
            dom: 'Bfrtip',
            language: {
                search: "" ,
                searchPlaceholder: "Search records",
            },
            buttons: [
                { text: 'Export to excel',extend: 'excel', className: 'btn-danger' ,exportOptions: {
                        columns: [ 0,1,2,3,4]
                    }},
                { text: 'Print',extend: 'pdf', className: 'btn btn-primary',exportOptions: {
                        columns: [ 0,1,2,3,4,5,6]
                    } },
            ],
            select: true
        } );
        // Restore state
        var state = table.state.loaded();
        if ( state ) {
          table.columns().eq( 0 ).each( function ( colIdx ) {
            var colSearch = state.columns[colIdx].search;

            if ( colSearch.search ) {
              $( 'input', table.column( colIdx ).footer() ).val( colSearch.search );
            }
          } );

          table.draw();
        }

    } );
</script>
