<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css">
<style type="text/css">
button.dt-button, div.dt-button, a.dt-button {
    background-image: linear-gradient(to bottom, #4cae4c 100%, #4cae4c 100%) !important;
    color:white;
    border-color: #4cae4c;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- For Messages -->
    <?php $this->load->view('admin/includes/_messages.php') ?>
    <div class="card">
      <div class="card-header">
        <div class="d-inline-block">
          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp;Fledge Loan List</h3>
        </div>
        <div class="d-inline-block float-right">
          <?php if($this->rbac->check_operation_permission('add')): ?>
            <a href="<?= base_url('admin/users/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_user') ?></a>
          <?php endif; ?>
        </div>
      </div>
    </div>
            <?php echo form_open(base_url('admin/report/pledge_report'),'id="form"','class="form-horizontal"');  ?>
              <div class="form-group row">

                         <input type="hidden" name="flag" class="form-control populate" id="flag"  required value="loan_search">

                        <div class="col-md-2">
                             <label class="col-md-12 control-label">From</label>
                            <input value="<?= $start_date; ?>" type="date" id="start_date" name="start_date" class="form-control populate" required>

                        </div>

                        <div class="col-md-2">
                            <label class="col-md-12 control-label">To</label>
                            <input value="<?= $end_date; ?>" type="date" id="end_date" name="end_date" class="form-control populate"  required>
                        </div>
                         <div class="col-md-2">
                             <label class="col-md-12 control-label"></label>

                            <button class="btn btn-primary" type="submit" style="margin-top: 11px;">Search</button>&nbsp;

                            <a href="<?= base_url('admin/report/pledge_report'); ?>" style="margin-top: 11px;" class="btn btn-danger" type="button">Clear</a>
                        </div>

                </div>
                  <?php echo form_close( ); ?>
    <div class="card">
      <div class="card-body table-responsive">
        <table id="naa_datatable" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>
              <th>Sl no</th>
              <th>Loan Id</th>
              <th>Customer Name</th>
              <th>Loan Amount[&#x20B9]</th>
              <th>Created on</th>


            </tr>
          </thead>
               <tbody>
           <?php //echo '<pre>';print_r($loanlist);exit;?>
            <?php foreach($loanlist as $key => $data): ?>

            <tr>
              <td><?=  $key +1; ?></td>
              <td><?= $data['loan_id']; ?></td>
              <td><?= $data['firstname']." ".$data['lastname']; ?></td>
              <td><?= $data['amount']; ?></td>
              <td><?= date_time($data['created_on']); ?></td>



            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script>
  $(document).ready(function() {
        var table = $('#naa_datatable').DataTable( {
            stateSave: true,
            dom: 'Bfrtip',
            language: {
                search: "" ,
                searchPlaceholder: "Search records",
            },
            buttons: [
                { text: 'Export to excel',extend: 'excel', className: 'btn-danger' ,exportOptions: {
                        columns: [ 0,1,2,3,4]
                    }},
                { text: 'Print',extend: 'pdf', className: 'btn-warning',exportOptions: {
                        columns: [ 0,1,2,3,4]
                    } },
            ],
            select: true
        } );
        // Restore state
        var state = table.state.loaded();
        if ( state ) {
          table.columns().eq( 0 ).each( function ( colIdx ) {
            var colSearch = state.columns[colIdx].search;

            if ( colSearch.search ) {
              $( 'input', table.column( colIdx ).footer() ).val( colSearch.search );
            }
          } );

          table.draw();
        }

    } );
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script type="text/javascript">
$.validator.addMethod("alpha", function(value, element) {
  return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
  // --                                    or leave a space here ^^
});
$.validator.addMethod("varcharRegex", function(value, element) {
      return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
  }, "Please enter character and number");

$.validator.addMethod("emailExt", function(value, element, param) {
  return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
},'Please enter a vaild email address');

$.validator.addMethod("varchardashRegex", function(value, element) {
      return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
  }, "Inventory Name must contain only letters, numbers, or dashes.");

$("#form").validate({

  onfocusout: false,
  invalidHandler: function(form, validator) {
      var errors = validator.numberOfInvalids();
      if (errors) {
          validator.errorList[0].element.focus();
      }
  } ,
error: function(label) {
   $(this).addClass("error");
 },
 rules: {
       start_date: {
          required: true,
      },
      end_date: {
         required: true,
     },
},
messages: {

start_date: {
          required:"Please enter Start Date",
      } ,
end_date: {
          required:"Please enter End Date",
} ,
  }

});

</script>
