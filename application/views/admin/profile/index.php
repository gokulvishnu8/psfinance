<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default color-palette-bo">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-pencil"></i>
              &nbsp; <?= trans('profile') ?> </h3>
          </div>
          <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/profile/change_pwd'); ?>" class="btn btn-success"><i class="fa fa-list"></i> <?= trans('change_password') ?></a>
          </div>
        </div>
        <div class="card-body">
           <!-- For Messages -->
            <?php $this->load->view('admin/includes/_messages.php') ?>

            <?php echo form_open(base_url('admin/profile'),'id="form"', 'class="form-horizontal"' )?>
              <div class="form-group">
                <label for="username" class="col-sm-2 control-label"><?= trans('username') ?></label>

                <div class="col-md-3">
                  <input type="text" name="username" value="<?= $admin['username']; ?>" class="form-control" id="username" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-sm-2 control-label"><?= trans('firstname') ?></label>

                <div class="col-md-3">
                  <input type="text" name="firstname" value="<?= $admin['firstname']; ?>" class="form-control" id="firstname" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <label for="lastname" class="col-sm-2 control-label"><?= trans('lastname') ?></label>

                <div class="col-md-3">
                  <input type="text" name="lastname" value="<?= $admin['lastname']; ?>" class="form-control" id="lastname" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <label for="email" class="col-sm-2 control-label"><?= trans('email') ?></label>

                <div class="col-md-3">
                  <input type="email" name="email" value="<?= $admin['email']; ?>" class="form-control" id="email" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="mobile_no" class="col-sm-2 control-label"><?= trans('mobile_no') ?></label>

                <div class="col-md-3">
                  <input type="number" name="mobile_no" value="<?= $admin['mobile_no']; ?>" class="form-control" id="mobile_no" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-3">
                  <input type="submit" name="submit" value="<?= trans('update_profile') ?>" class="btn btn-info pull-left">
                </div>
              </div>
            <?php echo form_close(); ?>
        </div>
        <!-- /.box-body -->
      </div>
    </section>
  </div>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
 <script>
   $(document).ready(function(){
     $('input[type=radio][name="contact_details"]').change(function(){
       if(this.value == 1) {
         $('#contact_details_div').show();
       }else{
         $('#contact_details_div').hide();
       }
     });
     $('input[type=radio][name="nominee_details"]').change(function(){
       if(this.value == 1) {
         $('#nominee_details_div').show();
       }else{
         $('#nominee_details_div').hide();
       }
     });
     $('input[type=radio][name="banking_details"]').change(function(){
       if(this.value == 1) {
         $('#banking_details_div').show();
       }else{
         $('#banking_details_div').hide();
       }
     });
   });
 </script>
<script type="text/javascript">
$.validator.addMethod("alpha", function(value, element) {
   return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
   // --                                    or leave a space here ^^
});
$.validator.addMethod("varcharRegex", function(value, element) {
       return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
   }, "Please enter character and number");

$.validator.addMethod("emailExt", function(value, element, param) {
   return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
},'Please enter a vaild email address');
$.validator.addMethod("varchardashRegex", function(value, element) {
       return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
   });
$.validator.addMethod("addressRegex", function(value, element) {
           return this.optional(element) || /^[a-z0-9\-\/\,\s]+$/i.test(value);
},"Special characters not allowed");
$.validator.addMethod("mobileValidation",function(value, element) {
 return !/^\d{8}$|^\d{10}$/.test(value) ? false : true;
},"Mobile number is invalid");
$("#form").validate({

   onfocusout: false,
   invalidHandler: function(form, validator) {
       var errors = validator.numberOfInvalids();
       if (errors) {
           validator.errorList[0].element.focus();
       }
   } ,
 error: function(label) {
    $(this).addClass("error");
  },
  rules: {
        username: {
           required: true,
           alpha:true
       },
        firstname: {
           required: true,
           alpha:true
       },
       lastname: {
          required: true,
          alpha:true
      },
       email: {
       emailExt: true,
       required: true
     },
     mobile_no: {
       required: true,
       maxlength: 10,
       mobileValidation:true
     },

 },
 messages: {
   username: {
             required:"Please enter Username",
               alpha:"Username must contain only letters"
         } ,
 firstname: {
           required:"Please enter First Name",
             alpha:"First Name must contain only letters"
       } ,
 lastname: {
       required: "Please enter Last Name",
       alpha:"Last Name must contain only letters"
     },
 email: {
       required: "Please enter Email address",
       emailExt:"Please enter a vaild email address"
   },
 mobile_no: {
       required: "Please enter Mobile Number",
       maxlength: "Please enter 10 digits"
     },

   }

});

</script>
