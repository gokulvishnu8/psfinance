<!-- Content Wrapper. Contains page content -->
<!-- <style>
.toggle-password {
  position: relative;
    bottom: 30px;
    left: 218px
}
.toggle-password1 {
  position: relative;
    bottom: 30px;
    left: 218px
}
</style> -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default color-palette-bo">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-pencil"></i>
              &nbsp; <?= trans('change_password') ?> </h3>
          </div>
        </div>
        <div class="card-body">
           <!-- For Messages -->
            <?php $this->load->view('admin/includes/_messages.php') ?>

            <?php echo form_open(base_url('admin/profile/change_pwd'),'id="form"', 'class="form-horizontal"');  ?>
              <div class="form-group">
                <label for="password" class="col-sm-3 control-label"><?= trans('new_password') ?></label>

                <div class="col-md-3 input-group">
                  <div class="input-group-prepend">
                   <div class="input-group-text"><i class="fa fa-eye-slash" id="eye"></i></div>
                 </div>
                  <input type="password" name="password" class="form-control" id="password" placeholder="" style="width: 200px;">
                </div>
              </div>

              <div class="form-group">
                <label for="confirm_pwd" class="col-sm-3 control-label"><?= trans('confirm_password') ?></label>
                <div class="col-md-3 input-group">
                  <div class="input-group-prepend">
                   <div class="input-group-text"><i class="fa fa-eye-slash" id="eye1"></i></div>
                 </div>
                  <input type="password" name="confirm_pwd" class="form-control" id="confirm_pwd" placeholder="" style="width: 200px;">
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-12">
                  <input type="submit" name="submit" value="<?= trans('change_password') ?>" class="btn btn-info pull-left">
                </div>
              </div>
            <?php echo form_close( ); ?>
        </div>
          <!-- /.box-body -->
      </div>
    </section>
  </div>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script type="text/javascript">
$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    // --                                    or leave a space here ^^
});
$.validator.addMethod("varcharRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
    }, "Please enter character and number");

 $.validator.addMethod("emailExt", function(value, element, param) {
    return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
},'Please enter a vaild email address');

$.validator.addMethod("varchardashRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
    }, "Inventory Name must contain only letters, numbers, or dashes.");

$("#form").validate({

    onfocusout: false,
    invalidHandler: function(form, validator) {
        var errors = validator.numberOfInvalids();
        if (errors) {
            validator.errorList[0].element.focus();
        }
    } ,
  error: function(label) {
     $(this).addClass("error");
   },
   rules: {
         password: {
            required: true,
            minlength: 8
        },
        confirm_pwd: {
      equalTo: "#password"
    }
  },
  messages: {

  password: {
            required:"Please enter New Password",
            minlength:"Length of Password should be atleast 8 Characters"
        },
  confirm_pwd: {
            required:"Please enter Confirm Password",
            equalTo:"Password and Confirm Password should be same",
        }
    }

});

</script>
<script>
$('#eye').click(function(){
if($(this).hasClass('fa-eye-slash')){
$(this).removeClass('fa-eye-slash');
$(this).addClass('fa-eye');
$('#password').attr('type','text');
}else{
$(this).removeClass('fa-eye');
$(this).addClass('fa-eye-slash');
$('#password').attr('type','password');
}
});
$('#eye1').click(function(){
if($(this).hasClass('fa-eye-slash')){
$(this).removeClass('fa-eye-slash');
$(this).addClass('fa-eye');
$('#confirm_pwd').attr('type','text');
}else{
$(this).removeClass('fa-eye');
$(this).addClass('fa-eye-slash');
$('#confirm_pwd').attr('type','password');
}
});
</script>
