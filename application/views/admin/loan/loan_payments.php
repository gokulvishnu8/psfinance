  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-plus"></i>
              <?php echo $title; ?> </h3>
          </div>
          <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/loan'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Loan List</a>
          </div>
        </div>
        <div class="card-body">

           <!-- For Messages -->
            <?php $this->load->view('admin/includes/_messages.php') ?>
            <?php $attributes = array('id' => 'getloanform','class' => 'form-horizontal'); ?>
            <?php echo form_open(base_url('admin/loan/loan_payments'), $attributes);  ?>
            <?php //echo '<pre>' ;print_r($interestData['today_gold_rate']);exit;?>
              <div class="form-group">
                <label for="inventory_name" class="col-md-2 control-label">Loan Number</label>

                <div class="col-md-3">
                    <select name="loannumber" id="single" class="form-control select2" style="width: 100%;">
                        <option value="">Please select</option>
                        <?php foreach ($loans as $key => $value): ?>
                        <option value="<?= $value['loan_id']; ?>"><?= $value['loan_id']; ?> - <?= $value['firstname']; ?> <?= $value['lastname']; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="form-group" style="float:left;">
                <div class="col-md-12">
                    <input type="button" name="getloandetails" id="getloandetails" value="Submit" class="btn btn-primary pull-right">
                </div>
            </div>
            <?php echo form_close( ); ?>
            <?php if($id != 0){ ?>
                <div class="col-md-12" style="clear:both;margin-bottom:20px">
                  <fieldset>
                      <legend>Customer Details</legend>
                      <table>
                          <tr>
                              <th style="font-size:22px"><?= $user_details['firstname']; ?> <?= $user_details['lastname']; ?></th>
                            </tr>
                            <tr>
                                <td><?= $user_details['address']; ?></td>
                            </tr>
                            <tr>
                                <td>Mob : <?= $user_details['mobile_no']; ?></td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div class="col-md-12" style="clear:both;margin-bottom:20px">
                    <fieldset>
                        <legend>Loan Details</legend>
                        <div>
                          <h5>Interest Due : <b><?= $days ?> days</b></h5>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dynamic_field">
                                <tr>
                                    <th width="10px">SL No.</th>
                                    <th width="40px">Item Image</th>
                                    <th width="180px">Selected Item</th>
                                    <th width="100px">Gross Weight(g)</th>
                                    <th width="100px">Net Weight(g)</th>
                                    <th width="100px">Quantity</th>
                                    <!-- <th width="140px">Price(&#x20b9)</th>
                                    <th width="140px">Interest(&#x20b9)</th>
                                    <th width="140px">Total(&#x20b9)</th> -->
                                </tr>
                                <?php foreach ($loan_inventory_details as $key => $value): ?>
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td><img src="<?= $value['image'] ?>" alt="" width="100%" style="display:block;margin:auto;"></td>
                                        <td><input readonly name="product_description[]" value=<?= $value['inventory_name'] ?> type="text" class="form-control name_list" /></td>
                                        <td><input readonly name="gross_weight[]" value=<?= $value['gross_weight'] ?> type="text" class="form-control name_list" /></td>
                                        <td><input readonly name="weight[]" value=<?= $value['weight'] ?> type="text" class="form-control name_list" /></td>
                                        <td><input readonly name="quantity[]" value=<?= $value['quantity'] ?> type="text" class="form-control name_list" /></td>
                                        <!-- <td><input readonly name="price[]" value=<?= round($value['price']) ?> type="text" class="form-control name_list" /></td>
                                        <td><input readonly id="interest<?= $key ?>" name="interest[]" value="0" type="text" class="form-control name_list" /></td>
                                        <td><input readonly id="total<?= $key ?>" name="total[]" value="0" type="text" class="form-control name_list" /></td> -->
                                    </tr>
                                <?php endforeach ?>
                            </table>
                        </div>
                        <div>
                          <h5 style="text-align:right">Pending Loan Amount : <b>₹ <input readonly id="pending_loan_amount" class="form-control name_list" value='<?= $amount ?>' style="display:inline-block;width:10%;font-weight:900"/></b></h5>
                        </div>
                    </fieldset>
                </div>
              <div class="col-md-12">
                  <fieldset>
                    <legend>Payments</legend>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dynamic_field">
                            <tr>
                                <th>Loan Amount</th>
                                <th style="text-align:right" colspan="2">₹ <?= number_format(ceil($amount),2); ?></td>
                            </tr>
                            <tr>
                                <th>Interest Due</th>
                                <th style="text-align:right" colspan="2">₹ <?= number_format(ceil($default_interest),2); ?></td>
                            </tr>
                            <tr>
                                <th>Misc Charges</th>
                                <th style="text-align:right" colspan="2">₹ <?= number_format(ceil($interest - $default_interest),2); ?></td>
                            </tr>
                            <tr>
                                <th>Service Charge</th>
                                <th style="text-align:right" colspan="2">₹ <input id="service_charge" style="text-align:right;width:50px" readonly value='<?= number_format(ceil(10),2); ?>' /></td>
                            </tr>
                            <tr>
                                <th style="font-size:25px">Grand Total</th>
                                <th style="text-align:right;font-size:25px" colspan="2">₹ <?= number_format(ceil($total) + 10,2); ?></td>
                            </tr>
                            <tr class="bg-dark">
                                <th style="font-size:20px">Payment Amount</th>
                                <th><input class="form-control" id="payment_amt"/>
                                  <label style="display: none;font-size: small !important;color: #e93b3b  !important;font-weight: 400 !important;" id="pay_error"  for="single">Please enter Payment Amount</label></th>
                                <th style="text-align:center"><button onclick="checkVal()" class="btn btn-success"   id="accept-btn">Accept Payment</button></th>
                            </tr>
                        </table>
                    </div>
                    <!-- Modal -->
                    <div id="confirm-delete" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modaltitle1">Confirm Payment</h4>
                        </div>
                        <div class="modal-body">
                            <p>Please confirm that the payment is accepted. After the payment is confirmed system will automatically modify the loan details. Click CLOSE if payment not done.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button class="btn btn-danger btn-ok" id="confirm_payment">Confirm</button>
                        </div>
                        </div>

                    </div>
                    </div>
                  </fieldset>
              </div>
            <?php } ?>
        </div>
          <!-- /.box-body -->
      </div>
    </section>
  </div>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
  <script>
      $(document).ready(function () {
          var id = <?php echo $id ?>;
          var customer_id = <?php echo $customer_id ?>;
          var real_total = <?php echo ceil($total) ?>;
          var total_amt = <?php echo ceil($amount) ?>;
          var total_interest = <?php echo ceil($interest) ?>;
          var default_interest = <?php echo ceil($default_interest) ?>;
          var days = <?php echo $days ?>;
          var service_charge = $('#service_charge').val();
          if(id > 0){
            $("#single").select2().select2('val',id);
          }else{
            $("#single").select2().select2('val','');
          }
          $('#getloandetails').click(function () {
              var loannumber = $('#single').val();
              var url = $('#getloanform').attr('action');
              $('#getloanform').attr('action', url+'/'+loannumber);
              $('#getloanform').submit();
          });
          $('#payment_amt').keyup(function () {
              var amt = $(this).val();
              var reduction_percentage = ((amt - total_interest - service_charge) / total_amt ) * 100;
              var price = $("input[name='price\\[\\]']").map(function(idx, elem) { return $(elem).val(); }).get();
              var pending_loan_amount = 0;
              $.each(price, function (indexInArray, valueOfElement) {
                var reduction_amount = valueOfElement * (reduction_percentage / 100);
                var balance = valueOfElement - reduction_amount;
                $('#interest'+indexInArray).val(reduction_amount.toFixed(2));
                $('#total'+indexInArray).val(balance.toFixed());
                pending_loan_amount = pending_loan_amount + balance;
              });
              $('#pending_loan_amount').val(pending_loan_amount.toFixed());
              if(amt <= 0){
                $('#accept-btn').addClass('disabled');
              }else{
                $('#accept-btn').removeClass('disabled');
                if(amt == (real_total + parseInt(service_charge))){
                  $('#accept-btn').text('Close Loan');
                  $('#accept-btn').removeClass('btn-success');
                  $('#accept-btn').addClass('btn-danger');
                }else{
                  $('#accept-btn').text('Accept Payment');
                  $('#accept-btn').addClass('btn-success');
                  $('#accept-btn').removeClass('btn-danger');
                }
              }

          });
          $('#confirm_payment').click(function (e) {
              e.preventDefault();
              var amt = $('#payment_amt').val();
              if(amt > (real_total + parseInt(service_charge))){
                $.notify("Amount entered exceed total payment amount", "error");
              }else if(amt == (real_total + parseInt(service_charge))){
                complete_payment(amt);
              }else if(amt < (real_total + parseInt(service_charge)) && amt >= 0){
                complete_payment(amt);
              }else{
                $.notify("Invalid Amount or Character Entered", "error");
              }
          });

          function complete_payment(amt){
            var service_charge = $('#service_charge').val();
            var product_description = $("input[name='product_description\\[\\]']").map(function(idx, elem) { return $(elem).val(); }).get();
            var weight = $("input[name='weight\\[\\]']").map(function(idx, elem) { return $(elem).val(); }).get();
            var quantity = $("input[name='quantity\\[\\]']").map(function(idx, elem) { return $(elem).val(); }).get();
            var price = $("input[name='price\\[\\]']").map(function(idx, elem) { return $(elem).val(); }).get();
            var interest = $("input[name='interest\\[\\]']").map(function(idx, elem) { return $(elem).val(); }).get();
            var total = $("input[name='total\\[\\]']").map(function(idx, elem) { return $(elem).val(); }).get();

            $.post('<?=base_url("admin/loan/complete_payment")?>',
            {
            '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
            id : id,
            customer_id : customer_id,
            amt : (amt - parseInt(total_interest)) - parseInt(service_charge),
            payment_amt : amt,
            total_amt : total_amt,
            product_description : product_description,
            weight : weight,
            quantity : quantity,
            price : price,
            interest : interest,
            total : total,
            service_charge : service_charge,
            total_interest : total_interest,
            default_interest : default_interest,
            days : days
            },
            function(data){
              var invoice_id = JSON.parse(data).invoice_id;
              let url = '<?= base_url('admin/invoices/invoice_pdf_download'); ?>/'+invoice_id;
              $.notify("Payment Successfull", "success");
              setTimeout(() => {
                window.location.href = url;
              }, 1000);
            // window.location.reload();
            });
          }
      });
  </script>

  <script type="text/javascript">
  $.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    // --                                    or leave a space here ^^
  });
  $.validator.addMethod("varcharRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
    }, "Please enter character and number");

  $.validator.addMethod("emailExt", function(value, element, param) {
    return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
  },'Please enter a vaild email address');

  $.validator.addMethod("varchardashRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
    }, "Inventory Name must contain only letters, numbers, or dashes.");

  $("#getloanform").validate({

    onfocusout: false,
    invalidHandler: function(form, validator) {
        var errors = validator.numberOfInvalids();
        if (errors) {
            validator.errorList[0].element.focus();
        }
    } ,
  error: function(label) {
     $(this).addClass("error");
   },
   rules: {
         loannumber: {
            required: true,
        },
  },
  messages: {

  loannumber: {
            required:"Please select Loan Number",
        } ,
    },
errorPlacement: function (error, element) {
if(element.hasClass('select2') && element.next('.select2-container').length) {
    error.insertAfter(element.next('.select2-container'));
}
else
{
     error.insertAfter(element);
}
},

  });

</script>
<script>
$('#single').change(function(){
var value=$(this).val();
if(value>0)
{
  $('#single-error').hide();
}
});
</script>
<script>
function checkVal()
{
  var textVal =$('#payment_amt').val();
 if (textVal == '')
  {
    $('#pay_error').show();
    $('#confirm-delete').modal('hide');
  }
  else {
    $('#pay_error').hide();
    $('#confirm-delete').modal('show');
  }
}
$("#payment_amt").keyup(function() {
    var $this = $(this);
    $this.val($this.val().replace(/[^\d.]/g, ''));
});
</script>
