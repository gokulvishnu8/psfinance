<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css"> 
<style type="text/css">
  button.dt-button, div.dt-button, a.dt-button {
background-color: green !important;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- For Messages -->
    <?php $this->load->view('admin/includes/_messages.php') ?>
    <div class="card">
      <div class="card-header">
        <div class="d-inline-block">
          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp; Loan List</h3>
        </div>
        <div class="d-inline-block float-right">
          <?php if($this->rbac->check_operation_permission('add')): ?>
            <a href="<?= base_url('admin/users/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_user') ?></a>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body table-responsive">
        <table id="na_datatable" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>
              <th>Sl no</th>
              <th>Loan Id</th>
              <th>Customer Name</th>
              <th>Loan Amount[&#x20B9]</th>
              <th>Created on</th>
              <th>Status</th>
              <th width="100" class="text-right">Action</th>
            </tr>
          </thead>
               <tbody>
           
            <?php foreach($loanlist as $key => $data): ?>
             <?php  $status = ($data['loan_status'] == 1)? 'checked': '';
              $progress = ($data['loan_progress'] == 'closed')? 'disabled': ''; ?>
            <tr>
              <td><?=  $key +1; ?></td>
              <td><?= $data['loan_id']; ?></td>
              <td><?= $data['firstname']." ".$data['lastname']; ?></td>
              <td><?= $data['amount']; ?></td>
              <td><?= date_time($data['created_on']); ?></td>
              <?php $trigger=updateStatus($data['created_on'],$data['updated_on'],$data['loan_id']);?>
              <td><input class="tgl_checkbox tgl-ios" data-id="<?= $data['loan_id']; ?>" 
                  id="cb_<?= $data['loan_id']; ?>" type="checkbox"  <?= $status; ?>><label for="cb_<?= $data['loan_id']; ?>"></label></td>

              <td>
                <a title="View" href="<?= base_url('admin/loan/view/'.$data['loan_id']); ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>  
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script>
  $(function () {
    $("#na_datatable").DataTable();
  });
</script> 
<script>
  //---------------------------------------------------//
  // var table = $('#na_datatable').DataTable( {
  //   "buttons": [{
  //     'extend': 'pdf',
  //     'text': 'Loan wise Report',
  //     'title': 'Loan wise report',
  //     'filename': 'loan_report',
  //     'exportOptions': {
  //                       columns: [ 0, 1, 2, 3,4]
  //                   },
  //     }],
  //   "processing": true,
  //   "serverSide": false,
  //   "ajax": "<?=base_url('admin/loan/datatable_json')?>",
  //   "order": [[6,'desc']],
  //   "dom": 'Bfrtip',
    
  //   "columnDefs": [
  //   { "targets": 0, "name": "id", 'searchable':true, 'orderable':true},
  //   { "targets": 1, "name": "loan_id", 'searchable':true, 'orderable':true},
  //   { "targets": 2, "name": " firstname", 'searchable':true, 'orderable':true},
  //    { "targets":3, "name": " amount", 'searchable':true, 'orderable':true},
  //   { "targets": 4, "name": " created_at", 'searchable':true, 'orderable':true},
  //   { "targets": 5, "name": "loan_status", 'searchable':true, 'orderable':true},
  //   { "targets": 6, "name": "Action", 'searchable':false, 'orderable':false,'width':'100px'}
  //   ]
  // });
</script>


<script type="text/javascript">
  $("body").on("change",".tgl_checkbox",function(){
    //console.log('checked');
    $.post('<?=base_url("admin/loan/change_status")?>',
    {

      '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
      loan_id : $(this).data('id'),
      status : $(this).is(':checked') == true?1:0
    },
    function(data){
      $.notify("Status Changed Successfully", "success");
    });
  });
</script>



