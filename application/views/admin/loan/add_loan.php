  <!-- Content Wrapper. Contains page content -->
  <style type="text/css">
    .take{
    width: 114px;
    height: 26px;
    line-height: 1px;
    margin-left: 7px;
    padding: 0px;
    }
    #results { padding:20px; border:1px solid; background:#ccc;}
  </style>
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-plus"></i>
              <?php echo $title; ?> </h3>
          </div>
          <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/loan'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Loan List</a>
          </div>
        </div>
        <div class="card-body">

           <!-- For Messages -->
            <?php $this->load->view('admin/includes/_messages.php') ?>

            <?php echo form_open(base_url('admin/loan/add'),'id="form"','enctype="multipart/form-data"' ,'class="form-horizontal"');  ?>
            <?php //echo '<pre>' ;print_r($interestData['today_gold_rate']);exit;?>
              <div class="form-group">
                <label for="inventory_name" class="col-md-2 control-label">Customer Name</label>

                <div class="col-md-3">
                  <select name="username" id="single" class="form-control select2" style="width: 100%;">
                    <option value="">Please select</option>
                    <?php foreach ($users as $key => $value): ?>
                       <option value="<?= $value['id']; ?>"><?= $value['firstname']; ?> <?= $value['lastname']; ?></option>
                    <?php endforeach ?>

                  </select>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-6">
                  <div id="my_camera"></div>
                </div>
                <div class="col-md-6" style="text-align: center;">
                  <div style="display: none;" id="results">Your captured image will appear here...</div>
                  <!-- <input type="button" style="display: none;margin: 5px;" class="btn btn-info" value="Submit" id="saveImage"> -->
                </div>
              </div>
              <div class="form-group">
                <label for="status" class="col-md-2 control-label">Add Items</label>
                  <div class="table-responsive">
                    <table class="table table-bordered" id="dynamic_field">
                    <tr>
                      <th width="200px">Select Item</th>
                      <th width="90px">Gross Weight(g)</th>
                      <th width="90px">Net Weight(g)</th>
                      <th width="210px">Purity</th>
                      <th width="90px">Quantity</th>
                      <th width="130px">Price(&#x20b9)</th>
                      <th width="220px">Image</th>
                      <th width="240px">Remark</th>
                      <th width="70px">Action</th>
                    </tr>
                      <tr>
                        <td>
                          <select name="items[0]" class="form-control" id="items">
                            <option value="">Select</option>
                             <?php foreach ($inventoryData as $key => $inventory): ?>
                            <option value="<?= $inventory['inventory_id']; ?>"><?= $inventory['inventory_name']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </td>
                        <td class="grosswghroot"><input type="text" name="grossweights[0]" placeholder=" " class="form-control name_list grosswghtcls" /></td>
                        <td class="wghroot"><input type="text" name="weights[0]" placeholder=" " class="form-control name_list wghtcls" /></td>
                         <td class="purityroot">
                          <select name="purities[0]" class="form-control puritycls">
                              <option value="">Select</option>
                              <option value="1">Halmark</option>
                              <option value="2">916</option>
                              <option value="3">Unsold</option>
                              <option value="4">22 Carat</option>
                              <option value="5">20 Carat</option>
                              <option value="6">18 Carat</option>
                            </select>
                        </td>
                        <td class="qtyroot"><input type="text" name="quantites[0]" placeholder=" " class="form-control name_list qtycls" /></td>


                        <td class="priceroot"><input readonly type="text" name="prices[0]" placeholder=" " class="form-control name_list price_seg" /></td>
                        <td><div class="imgDisplay" style="margin-bottom:8px;"></div><i onClick="start_snapshot()" title="Start Webcam" class="fa fa-camera" style="font-size:24px;cursor: pointer;"></i><input type=button value="Take Snapshot" class="btn btn-warning take" onClick="take_snapshot()" style="margin-bottom:8px;"><input class="form-control" type="hidden" name="imgData[]"></td>
                        <td><textarea name="remarks[0]" placeholder=" " class="form-control name_list"></textarea></td>
                        <td><button type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus"></i></button></td>
                      </tr>
                    </table>
                  </div>
              </div>

               <div class="form-group">
                <label for="status" class="col-md-2 control-label">Net Amount(&#x20b9)</label>
                <div class="col-md-3">
                    <input readonly type="text" id="net_price" name="net_price" value="0" placeholder=" " class="form-control net_price_cal" />
                </div>
              </div>
              <div class="form-group">
               <label for="status" class="col-md-2 control-label">Given Loan Amount(&#x20b9)</label>
               <div class="col-md-3">
                   <input  type="text" id="given_loan_price" name="given_loan_price" value="" placeholder=" " class="form-control" />
               </div>
             </div>
             <div class="form-group">
              <label for="status" class="col-md-2 control-label">Loan Created Date</label>
              <div class="col-md-3">
                  <input  type="date" id="created_at" name="created_at" value="" placeholder=" " class="form-control" />
              </div>
            </div>
              <div class="form-group" style="float:left;">
                <div class="col-md-12">
                  <input type="submit" name="submit" value="Submit" class="btn btn-primary pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
        </div>
          <!-- /.box-body -->
      </div>
    </section>
  </div>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<script>
$(document).ready(function(){
  var i=1;
  var inventory = <?php echo json_encode($inventoryData); ?>;
  $('#add').click(function(){
    i++;
var first='<tr id="row'+i+'"><td><select name="items['+(i-1)+']" class="form-control" id="items"><option value="">Select</option>';
var getOption ='';
for (let i = 0; i < inventory.length; i++) {
 getOption += '<option value="'+inventory[i].inventory_id+'">'+inventory[i].inventory_name+'</option>';
}
var option =getOption;
var last='</select></td><td class="grosswghroot"><input type="text" name="grossweights['+(i-1)+']" placeholder=" " class="form-control name_list grosswghtcls" /></td><td class="wghroot"><input type="text" name="weights['+(i-1)+']" placeholder=" " class="form-control name_list wghtcls" /></td><td class="purityroot"><select name="purities['+(i-1)+']" class="form-control puritycls"><option value="">Select</option><option value="1">Halmark</option><option value="2">916</option><option value="3">Unsold</option><option value="4">22 Carat</option><option value="5">20 Carat</option><option value="6">18 Carat</option></select></td><td class="qtyroot"><input type="text" name="quantites['+(i-1)+']" placeholder=" " class="form-control name_list qtycls" /></td><td class="priceroot"><input readonly type="text" name="prices['+(i-1)+']" placeholder=" " class="form-control name_list price_seg" /></td><td><div class="imgDisplay" style="margin-bottom:8px;"></div><i onClick="start_snapshot()" title="Start Webcam" class="fa fa-camera" style="font-size:24px;cursor: pointer;"></i><input type=button value="Take Snapshot" class="btn btn-warning take" onClick="take_snapshot()" style="margin-bottom:8px;"><input type="hidden" name="imgData[]"></td><td><textarea name="remarks['+(i-1)+']" placeholder=" " class="form-control name_list"></textarea></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class="fa fa-close"></i></button></td></tr>';

    $('#dynamic_field').append(first+option+last);
    $('select[name="items['+(i-1)+']"]').rules("add", {required: true});
    $('input[name="grossweights['+(i-1)+']"]').rules("add", {required: true,messages:{required:'Please enter Gross Weight.'}});
    $('input[name="weights['+(i-1)+']"]').rules("add", {required: true,messages:{required:'Please enter Net Weight.'}});
    $('select[name="purities['+(i-1)+']"]').rules("add", {required: true});
    $('input[name="quantites['+(i-1)+']"]').rules("add", {required: true,messages:{required:'Please enter Quantity.'}});
    // $('textarea[name="remarks['+(i-1)+']"]').rules("add", {required: true,messages:{required:'Please enter Remark.'}});
    $('.grosswghtcls').each(function () {
        $(this).rules("add", {
            required: true
        });
    });
  });

  $(document).on('click', '.btn_remove', function(){
    var button_id = $(this).attr("id");
    $('#row'+button_id+'').remove();
  });

  $('#submit').click(function(){
    $.ajax({
      url:"name.php",
      method:"POST",
      data:$('#add_name').serialize(),
      success:function(data)
      {
        //alert(data);
        $('#add_name')[0].reset();
      }
    });
  });

});
</script>
<script type="text/javascript">
$(document).on('keyup', '.qtycls', function (e) {
    var weight     = $(this).parent().siblings('.wghroot').children().val();
    var quantity   = $(this).val();
    var purity     = $(this).parent().siblings('.purityroot').children().val();
    var gram_price = '<?php echo $interestData['today_gold_rate']?>';
    if(purity=='1')
    {
      var actual_gold_rate=gram_price;
    }
    else if(purity=='2')
    {
      var actual_gold_rate=.98*gram_price;
    }
    else if(purity=='3')
    {
      var actual_gold_rate=.95*gram_price;
    }
    else if(purity=='4')
    {
      var actual_gold_rate=.90*gram_price;
    }
    else if(purity=='5')
    {
      var actual_gold_rate=.87*gram_price;
    }
    else if(purity=='6')
    {
      var actual_gold_rate=.70*gram_price;
    }
    var net_price  =  quantity * actual_gold_rate * weight;
    $(this).parent().siblings('.priceroot').children().val(Math.round(net_price));
	/*var sum = 0;
	var starter =$('#net_price').val();
    var value = Math.round(net_price);
    if(!isNaN(value)) {
        sum =value +parseInt(starter);
		$('#net_price').val(sum);
    }*/
	var sum_array =[];
	$(".price_seg").each(function() {
        var z= $(this).val();
		sum_array.push(z);
    });
	var total = 0;
	for (var i in sum_array) {
		total += 	parseInt(sum_array[i]);
	}
	$('#net_price').val(total);
});
</script>
<script type="text/javascript">
$(document).on('keyup', '.wghtcls', function (e) {
    $(this).parent().siblings('.priceroot').children().val(" ");
    $(this).parent().siblings('.qtyroot').children().val(" ");
    $(this).parent().siblings('.purityroot').children().prop('selectedIndex',0);
});
</script>
<script type="text/javascript">
$(document).on('change', '.puritycls', function (e) {
    $(this).parent().siblings('.qtyroot').children().val(" ");
    $(this).parent().siblings('.priceroot').children().val(" ");
});
</script>
<script language="JavaScript">
    Webcam.set({
        width: 490,
        height: 390,
        image_format: 'jpeg',
        jpeg_quality: 90
    });

   // Webcam.attach( '#my_camera' );

    //function take_snapshot() {
        //Webcam.snap( function(data_uri) {
          //  $(".image-tag").val(data_uri);
          // document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
            //$(this).siblings('.sampleImg').val(4);
            //console.log($(this).siblings('.sampleImg').children())
            //$(this).siblings('input').val(2);
            //document.getElementsByClassName('sampleImg').innerHTML = '<img  width="50" height="40" src="'+data_uri+'"/>';
            //$('#saveImage').show();
       // } );
    //}
    $(document).on('click', '.take', function (e) {
          Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
           document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';

            //document.getElementsByClassName('sampleImg').innerHTML = '<img  width="50" height="40" src="'+data_uri+'"/>';
            //$('#saveImage').show();
        } );
          var imgUrl =$('#results').children('img').attr('src');
          $(this).siblings('input').val(imgUrl);
          if (typeof imgUrl !== "undefined") {
              $(this).siblings('.imgDisplay').html('<img  width="100%" src="'+imgUrl+'"/>');
          }

          //document.getElementsByClassName('imgDisplay').innerHTML = '<img  width="50" height="40" src="'+imgUrl+'"/>';
    } );
    function start_snapshot() {
        Webcam.attach( '#my_camera' );
        $('#results').show();

    }

</script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
 <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
  <script>
    $(document).ready(function(){
      $('input[type=radio][name="contact_details"]').change(function(){
        if(this.value == 1) {
          $('#contact_details_div').show();
        }else{
          $('#contact_details_div').hide();
        }
      });
      $('input[type=radio][name="nominee_details"]').change(function(){
        if(this.value == 1) {
          $('#nominee_details_div').show();
        }else{
          $('#nominee_details_div').hide();
        }
      });
      $('input[type=radio][name="banking_details"]').change(function(){
        if(this.value == 1) {
          $('#banking_details_div').show();
        }else{
          $('#banking_details_div').hide();
        }
      });
    });
  </script>
<script type="text/javascript">
$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    // --                                    or leave a space here ^^
});
$.validator.addMethod("loginRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
    }, "Please enter character and number");

 $.validator.addMethod("emailExt", function(value, element, param) {
    return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
},'Please enter a vaild email address');
$("#form").validate({

    onfocusout: false,
    invalidHandler: function(form, validator) {
        var errors = validator.numberOfInvalids();
        if (errors) {
            validator.errorList[0].element.focus();
        }
    } ,
  error: function(label) {
     $(this).addClass("error");
   },
   rules: {
         username: {
            required: true,
        },
         'items[0]':{
         required: true,
       },
       'grossweights[0]': {
        required: true
      },
       'weights[0]': {
        required: true,
      },
      'purities[0]': {
        required: true,
      },
     'quantites[0]': {
        required: true,
      },
      // 'remarks[0]': {
      //   required: true,
      // },
      'imgData[]': {
        required: true,
      },
      given_loan_price:{
        required: true,
      },
      created_at:{
        required: true,
      }
  },
  messages: {

  username: {
            required:"Please select Customer Name",
        } ,
 'items[0]': {

        required: "Please select Item",

      },
 'grossweights[0]': {

        required: "Please enter Gross Weight",

      },
 'weights[0]': {

        required: "Please enter Net Weight",

      },
  'purities[0]': {

        required: "Please select Purity",

      },
  'quantites[0]': {

        required: "Please enter Quantity",

      },
      // 'remarks[0]': {
      //   required: "Please enter Remark",
      // },
      'imgData[]': {
        required: "Please take Snapshot",
      },
      given_loan_price:{
        required:"Please enter Given Loan amount",
      },
      created_at:{
        required: "Please select Loan Created Date",
      }
    },
        errorPlacement: function (error, element) {
    if(element.hasClass('select2') && element.next('.select2-container').length) {
        error.insertAfter(element.next('.select2-container'));
    }
    else
    {
         error.insertAfter(element);
    }
    },

});

</script>
<script>
// $("#add").click(function (e) {
//   alert(123)
// $('.grosswghtcls').each(function () {
//     $(this).rules("add", {
//         required: true
//     });
// });
// e.preventDefault();
// });
</script>
