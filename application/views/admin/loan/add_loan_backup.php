  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-plus"></i>
              <?php echo $title; ?> </h3>
          </div>
          <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/loan'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Loan List</a>
          </div>
        </div>
        <div class="card-body">
   
           <!-- For Messages -->
            <?php $this->load->view('admin/includes/_messages.php') ?>

            <?php echo form_open(base_url('admin/loan/add'),'enctype="multipart/form-data"' ,'class="form-horizontal"');  ?> 
            <?php //echo '<pre>' ;print_r($interestData['today_gold_rate']);exit;?>
              <div class="form-group">
                <label for="inventory_name" class="col-md-2 control-label">Customer Name</label>

                <div class="col-md-3">
                  <select name="username" id="single" class="form-control select2" style="width: 100%;">
                    <option value="">Please select</option>
                    <?php foreach ($users as $key => $value): ?>
                       <option value="<?= $value['id']; ?>"><?= $value['firstname']; ?> <?= $value['lastname']; ?></option>
                    <?php endforeach ?>
                   
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="status" class="col-md-2 control-label">Add Items</label>
                  <div class="table-responsive">
                    <table class="table table-bordered" id="dynamic_field">
                    <tr>
                      <th width="200px">Select Item</th>
                      <th width="90px">Weight(g)</th>
                      <th width="90px">Quantity</th>
                      <th width="100px">Price(&#x20b9)</th>
                      <th width="220px">Image</th>
                      <th width="240px">Remark</th>
                      <th width="100px">Action</th>
                    </tr>
                      <tr>
                        <td>
                          <select name="items[]" class="form-control" id="items">
                            <option value="">Select</option>
                             <?php foreach ($inventoryData as $key => $inventory): ?>
                            <option value="<?= $inventory['inventory_id']; ?>"><?= $inventory['inventory_name']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </td>
                        <td class="wghroot"><input type="text" name="weights[]" placeholder=" " class="form-control name_list wghtcls" /></td>
                        <td class="qtyroot"><input type="text" name="quantites[]" placeholder=" " class="form-control name_list qtycls" /></td>
                        <td class="priceroot"><input readonly type="text" name="prices[]" placeholder=" " class="form-control name_list" /></td>
                        <td><input  type="file" name="images[]" placeholder=" " class="form-control name_list" /></td>
                        <td><textarea name="remarks[]" placeholder=" " class="form-control name_list"></textarea></td>
                        <td><button type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus"></i></button></td>
                      </tr>
                    </table>
                  </div>
              </div>
              
              <div class="form-group">
                <label for="status" class="col-md-2 control-label">Status</label>
                <div class="col-md-3">
                    <select name="status" class="form-control" id="status">
                      <option value="">Select</option>
                      <option value="1">Active</option>
                      <option value="0">Inactive</option>
                    </select>
                </div>
              </div>
              <div class="form-group" style="float:left;">
                <div class="col-md-12">
                  <input type="submit" name="submit" value="Submit" class="btn btn-primary pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
        </div>
          <!-- /.box-body -->
      </div>
    </section> 
  </div>
<script>
$(document).ready(function(){
  var i=1;
  var inventory = <?php echo json_encode($inventoryData); ?>;
  $('#add').click(function(){
    i++;
var first='<tr id="row'+i+'"><td><select name="items[]" class="form-control" id="items"><option value="">Select</option>';
var getOption ='';
for (let i = 0; i < inventory.length; i++) {
 getOption += '<option value="'+inventory[i].inventory_id+'">'+inventory[i].inventory_name+'</option>';
}
var option =getOption;
var last='</select></td><td class="wghroot"><input type="text" name="weights[]" placeholder=" " class="form-control name_list wghtcls" /></td><td class="qtyroot"><input type="text" name="quantites[]" placeholder=" " class="form-control name_list qtycls" /></td><td class="priceroot"><input readonly type="text" name="prices[]" placeholder=" " class="form-control name_list" /></td><td><input  type="file" name="images[]" placeholder=" " class="form-control name_list" /></td><td><textarea name="remarks[]" placeholder=" " class="form-control name_list"></textarea></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class="fa fa-close"></i></button></td></tr>';

    $('#dynamic_field').append(first+option+last);
  });
  
  $(document).on('click', '.btn_remove', function(){
    var button_id = $(this).attr("id"); 
    $('#row'+button_id+'').remove();
  });
  
  $('#submit').click(function(){    
    $.ajax({
      url:"name.php",
      method:"POST",
      data:$('#add_name').serialize(),
      success:function(data)
      {
        //alert(data);
        $('#add_name')[0].reset();
      }
    });
  });
  
});
</script>
<script type="text/javascript">
$(document).on('keyup', '.qtycls', function (e) {
    var weight     = $(this).parent().siblings('.wghroot').children().val();
    var quantity   = $(this).val();
    var gram_price = '<?php echo $interestData['today_gold_rate']?>';
    var net_price  =  quantity * gram_price * weight;
    $(this).parent().siblings('.priceroot').children().val(net_price);
});
</script>
<script type="text/javascript">
$(document).on('keyup', '.wghtcls', function (e) {
    $(this).parent().siblings('.priceroot').children().val(" ");
    $(this).parent().siblings('.qtyroot').children().val(" ");
});
</script>