  <!-- Content Wrapper. Contains page content -->
  <style type="text/css">
    #overlay{
  position: fixed;
  top:0;
  left:0;
  width:100%;
  height:100%;
  background: rgba(0,0,0,0.8) none 50% / contain no-repeat;
  cursor: pointer;
  transition: 0.3s;

  visibility: hidden;
  opacity: 0;
}
#overlay.open {
  visibility: visible;
  opacity: 1;
}

#overlay:after { /* X button icon */
  content: "\2715";
  position: absolute;
  color:#fff;
  top: 10px;
  right:20px;
  font-size: 2em;
}
img{
  cursor: pointer;
}
  </style>
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-eye"></i>
              &nbsp;  <?php echo $title; ?> </h3>
          </div>
          <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/loan'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Loan List</a>
            <a href="<?= base_url('admin/loan/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i>Add Loan</a>
            <a class="btn btn-primary create_pdf"
href="<?= base_url('admin/loan/loan_pdf_download/'.$loanData['loan_id']); ?>"><i class="fa fa-print"></i>Print</a>
          </div>
        </div>
        <div class="card-body">

           <!-- For Messages -->
            <?php $this->load->view('admin/includes/_messages.php') ?>

            <?php echo form_open(base_url('admin/loan/edit/'.$loanData['loan_id']), 'class="form-horizontal"' )?>

              <div class="form-group">
                <label for="inventory_name" class="col-md-2 control-label">Customer Name</label>

                <div class="col-md-3">
                  <select disabled name="username" id="single" class="form-control select2" style="width: 100%;">
                    <option value="">Please select</option>
                    <?php foreach ($users as $key => $value): ?>
                       <option <?php if($value['id']==$loanData['customer_id']) { echo 'selected';} ?> value="<?= $value['id']; ?>"><?= $value['firstname']; ?> <?= $value['lastname']; ?></option>
                    <?php endforeach ?>

                  </select>
                </div>
              </div>
              <?php //echo '<pre>';print_r($loanInventory[0]);exit; ?>
               <div class="form-group">
                <label for="status" class="col-md-2 control-label">Add Items</label>
                  <div class="table-responsive">
                    <table class="table table-bordered" id="dynamic_field">
                    <tr>
                      <th width="210px">Selected Item</th>
                      <th width="90px">Gross Weight(g)</th>
                      <th width="90px">Net Weight(g)</th>
                      <th width="210px">Purity</th>
                      <th width="90px">Quantity</th>
                      <th width="120px">Price(&#x20b9)</th>
                      <th width="200px">Image</th>
                      <th width="250px">Remark</th>
                     <!--  <th>Action</th> -->
                    </tr>
                      <tr>
                        <td>
                          <select disabled name="items[]" class="form-control" id="items">
                            <option value="">Select</option>
                             <?php foreach ($inventoryData as $key => $inventory): ?>
                            <option <?php if($loanInventory[0]['inventory_id']==$inventory['inventory_id']) { echo 'selected';} ?> value="<?= $inventory['inventory_id']; ?>"><?= $inventory['inventory_name']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </td>
                        <td><input  readonly value="<?= $loanInventory[0]['gross_weight']; ?>" type="text" name="grossweights[]" placeholder="" class="form-control name_list" /></td>
                        <td><input  readonly value="<?= $loanInventory[0]['weight']; ?>" type="text" name="weights[]" placeholder="Enter Weight" class="form-control name_list" /></td>
                        <td class="purityroot">
                          <select disabled name="purities[]" class="form-control puritycls">
                              <option value="">Select</option>
                              <option <?php if($loanInventory[0]['weight']=='1') { echo 'selected';}?> value="1">Halmark</option>
                              <option <?php if($loanInventory[0]['weight']=='2') { echo 'selected';}?> value="2">916</option>
                              <option <?php if($loanInventory[0]['weight']=='3') { echo 'selected';}?> value="3">Unsold</option>
                              <option <?php if($loanInventory[0]['weight']=='4') { echo 'selected';}?> value="4">22 Carat</option>
                              <option <?php if($loanInventory[0]['weight']=='5') { echo 'selected';}?> value="5">20 Carat</option>
                              <option <?php if($loanInventory[0]['weight']=='6') { echo 'selected';}?> value="6">18 Carat</option>
                            </select>
                        </td>
                        <td><input readonly value="<?= $loanInventory[0]['quantity']; ?>" type="text" name="quantites[]" placeholder="Enter Quantity" class="form-control name_list" /></td>
                        <td><input readonly value="<?= $loanInventory[0]['price']; ?>" type="text" name="prices[]" placeholder="Enter Price" class="form-control name_list" /></td>

                        <td><?php if(!empty($loanInventory[0]['image'])) { ?>
                          <img src="<?php echo $loanInventory[0]['image'] ?>"  width="100%">
                       <?php  } else { ?>

                     <?php   } ?></td>
                        <td><textarea readonly name="remarks[]" placeholder="Enter Remark" class="form-control name_list"><?= $loanInventory[0]['remark']; ?></textarea></td>

                      </tr>

                      <?php  for ($x = 1; $x < count($loanInventory); $x++) { ?>
                      <tr id="row2">
                        <td>
                          <select disabled name="items[]" class="form-control" id="items">
                            <option value="">Select</option>
                             <?php foreach ($inventoryData as $key => $inventory): ?>
                            <option <?php if($loanInventory[$x]['inventory_id']==$inventory['inventory_id']) { echo 'selected';} ?> value="<?= $inventory['inventory_id']; ?>"><?= $inventory['inventory_name']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </td>
                        <td><input  readonly value="<?= $loanInventory[$x]['gross_weight']; ?>" type="text" name="grossweights[]" placeholder="" class="form-control name_list" /></td>
                        <td>
                          <input readonly value="<?= $loanInventory[$x]['weight']; ?>" type="text" name="weights[]" placeholder="Enter Weight" class="form-control name_list">
                        </td>
                         <td class="purityroot">
                          <select disabled name="purities[]" class="form-control puritycls">
                              <option value="">Select</option>
                              <option <?php if($loanInventory[$x]['weight']=='1') { echo 'selected';}?> value="1">Halmark</option>
                              <option <?php if($loanInventory[$x]['weight']=='2') { echo 'selected';}?> value="2">916</option>
                              <option <?php if($loanInventory[$x]['weight']=='3') { echo 'selected';}?> value="3">Unsold</option>
                              <option <?php if($loanInventory[$x]['weight']=='4') { echo 'selected';}?> value="4">22 Carat</option>
                              <option <?php if($loanInventory[$x]['weight']=='5') { echo 'selected';}?> value="5">20 Carat</option>
                              <option <?php if($loanInventory[$x]['weight']=='6') { echo 'selected';}?> value="6">18 Carat</option>
                            </select>
                        </td>

                        <td>
                            <input readonly value="<?= $loanInventory[$x]['quantity']; ?>" type="text" name="quantites[]" placeholder="Enter Quantity" class="form-control name_list">
                        </td>
                        <td>
                            <input readonly value="<?= $loanInventory[$x]['price']; ?>" type="text" name="prices[]" placeholder="Enter Price" class="form-control name_list">
                        </td>
                        <td>
                          <?php if(!empty($loanInventory[$x]['image'])) {?>
                          <img src="<?php echo $loanInventory[$x]['image'] ?>" width="100%">
                        <?php } else {?>

                        <?php } ?>
                        </td>
                        <td>
                          <textarea readonly name="remarks[]" placeholder="Enter Remark" class="form-control name_list"><?= $loanInventory[$x]['remark']; ?></textarea>
                        </td>

                      </tr>

                    <?php } ?>



                    </table>
                  </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">Net Amount(&#x20b9)</label>

                <div class="col-md-3">
                  <input readonly type="text" name="firstname" value="<?= $loanData['loan_amount']; ?>" class="form-control" id="firstname" placeholder="">
                </div>
              </div>
              <div class="form-group">
               <label for="status" class="col-md-2 control-label">Given Loan Amount(&#x20b9)</label>
               <div class="col-md-3">
                   <input  readonly type="text" value="<?= $loanData['given_loan_amount']; ?>" id="given_loan_amount" name="given_loan_amount" value="" placeholder=" " class="form-control" />
               </div>
             </div>
             <?php
                $createDate = new DateTime($loanData['created_at']);
                $strip = $createDate->format('Y-m-d');
             ?>
             <div class="form-group">
              <label for="status" class="col-md-2 control-label">Loan Created Date</label>
              <div class="col-md-3">
                  <input  type="date" readonly value="<?= $strip; ?>" id="created_at" name="created_at"  placeholder=" " class="form-control" />
              </div>
            </div>
               <div class="form-group">
                <label for="status" class="col-md-2 control-label">Loan Status</label>
                <div class="col-md-3">
                  <?php if($loanData['loan_progress']=='auction'){ ?>
                        <label style="color:red">Auction</label>
                     <?php } else if($loanData['loan_progress']='ongoing'){ ?>
                        <label style="color:green">Ongoing</label>
                   <?php } ?>
                </div>
              </div>
              <div id="overlay"></div>
             <!--  <div class="form-group" style="float:left;">
                <div class="col-md-12">
                  <input type="submit" name="submit" value="Submit" class="btn btn-primary pull-right">
                </div>
              </div> -->
            <?php echo form_close(); ?>
        </div>
          <!-- /.box-body -->
      </div>
    </section>
  </div>
  <script>
$(document).ready(function(){
  var i=1;
  var inventory = <?php echo json_encode($inventoryData); ?>;
  console.log(inventory);
  $('#add').click(function(){
    i++;
var first='<tr id="row'+i+'"><td><select name="items[]" class="form-control" id="items"><option value="">Select</option>';
var getOption ='';
for (let i = 0; i < inventory.length; i++) {
 getOption += '<option value="'+inventory[i].inventory_id+'">'+inventory[i].inventory_name+'</option>';
}
var option =getOption;
var last='</select></td><td><input type="text" name="weights[]" placeholder="Enter Weight" class="form-control name_list" /></td><td><input type="text" name="quantites[]" placeholder="Enter Quantity" class="form-control name_list" /></td><td><input type="text" name="prices[]" placeholder="Enter Price" class="form-control name_list" /></td><td><textarea name="remarks[]" placeholder="Enter Remark" class="form-control name_list"></textarea></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class="fa fa-close"></i></button></td></tr>';

    $('#dynamic_field').append(first+option+last);
  });

  $(document).on('click', '.btn_remove', function(){
    var button_id = $(this).attr("id");
    $('#row'+button_id+'').remove();
  });

  $('#submit').click(function(){
    $.ajax({
      url:"name.php",
      method:"POST",
      data:$('#add_name').serialize(),
      success:function(data)
      {
        alert(data);
        $('#add_name')[0].reset();
      }
    });
  });

});
</script>
<script type="text/javascript">
  $('img').on('click', function() {
  $('#overlay')
    .css({backgroundImage: `url(${this.src})`})
    .addClass('open')
    .one('click', function() { $(this).removeClass('open'); });
});
</script>
