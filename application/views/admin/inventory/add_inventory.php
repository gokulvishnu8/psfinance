  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-plus"></i>
              <?php echo $title; ?> </h3>
          </div>
          <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/inventory'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Inventory List</a>
          </div>
        </div>
        <div class="card-body">

           <!-- For Messages -->
            <?php $this->load->view('admin/includes/_messages.php') ?>

            <?php echo form_open(base_url('admin/inventory/add'),'id="form"', 'class="form-horizontal"');  ?>

              <div class="form-group">
                <label for="inventory_name" class="col-md-2 control-label">Inventory Name</label>

                <div class="col-md-3">
                  <input type="text" name="inventory_name" class="form-control" id="inventory_name" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <label for="status" class="col-md-2 control-label">Status</label>
                <div class="col-md-3">
                    <select name="status" class="form-control" id="status">
                      <option value="">Select</option>
                      <option value="1">Active</option>
                      <option value="0">Inactive</option>
                    </select>
                </div>
              </div>
              <div class="form-group" style="float:left;">
                <div class="col-md-12">
                  <input type="submit" name="submit" value="Submit" class="btn btn-primary pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
        </div>
          <!-- /.box-body -->
      </div>
    </section>
  </div>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script type="text/javascript">
$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    // --                                    or leave a space here ^^
});
$.validator.addMethod("varcharRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
    }, "Please enter character and number");

 $.validator.addMethod("emailExt", function(value, element, param) {
    return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
},'Please enter a vaild email address');

$.validator.addMethod("varchardashRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
    }, "Inventory Name must contain only letters, numbers, or dashes.");

$("#form").validate({

    onfocusout: false,
    invalidHandler: function(form, validator) {
        var errors = validator.numberOfInvalids();
        if (errors) {
            validator.errorList[0].element.focus();
        }
    } ,
  error: function(label) {
     $(this).addClass("error");
   },
   rules: {
         inventory_name: {
            required: true,
            varchardashRegex:true
        },
         status: {
            required: true,
        },
  },
  messages: {

  inventory_name: {
            required:"Please enter Inventory Name",
        } ,
  status: {
        required: "Please select Status",
      },

    }

});

</script>
