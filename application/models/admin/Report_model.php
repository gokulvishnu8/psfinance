<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model{

	public function get_all_users(){
			$this->db->select('*');
			$this->db->where('is_active', 1);
			return $this->db->get('ci_users')->result_array();
	}
	public function get_all_inventory(){
			$this->db->select('*');
			return $this->db->get('inventory')->result_array();
	}
	public function add_loan($data){
			$this->db->insert('loan', $data);
			$insert_id = $this->db->insert_id();
   			return  $insert_id;
	}
	public function add_loan_inventory($loanInventorydata){
			$this->db->insert('loan_inventory_details', $loanInventorydata);
			$insert_id = $this->db->insert_id();
   			return  $insert_id;
	}
	public function get_all_loan(){
			$this->db->select('*');
			$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
			return $this->db->get('loan')->result_array();
	}
	public function change_status()
		{	
			$this->db->set('loan_status', $this->input->post('status'));
			$this->db->where('loan_id', $this->input->post('loan_id'));
			$this->db->update('loan');
		} 
	public function get_loan_by_id($id){
			$query = $this->db->get_where('loan', array('loan_id' => $id));
			return $result = $query->row_array();
	}
	public function get_loan_inventory_id($id){
			$query = $this->db->get_where('loan_inventory_details', array('loan_ID' => $id));
			return $result = $query->result_array();
	}
	public function update_total_amount($data, $id){
		    $this->db->set('amount',$data);
			$this->db->where('loan_id', $id);
			$this->db->update('loan');
			return true;
	}
	public function get_all_interestInfo(){
			$query = $this->db->get_where('interest_rate', array('id' => 1));
			return $result = $query->row_array();
	}
	// Get user detial by ID
	public function get_user_by_id($id){
			$query = $this->db->get_where('ci_users', array('id' => $id));
			return $result = $query->row_array();
		}
	public function get_loan_by_userid($id){
			$query = $this->db->get_where('loan', array('customer_id' => $id));
			return $result = $query->result_array();
	}
	public function get_loan_inventory($loan_id)
	{
	$this->db->select('*');
	$this->db->from('loan_inventory_details');
	$this->db->join('inventory','inventory.inventory_id = loan_inventory_details.inventory_id','left');
	$this->db->where('loan_inventory_details.loan_ID', $loan_id);
	$result = $this->db->get();
	return $result->result_array();
	}
	public function get_all_closed_loan(){
			$this->db->select('*,loan.updated_at as closed_on');
			$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
			$this->db->where('loan.loan_progress', 'closed');
			return $this->db->get('loan')->result_array();
	}
	public function get_all_closed_loan_with_sum($id){
			$this->db->select_sum('ci_payments.default_interest');
			$this->db->join('ci_payments', 'ci_payments.loanid = loan.loan_id');
			$this->db->where('loan.loan_progress', 'closed');
			return $this->db->get('loan')->row_array();
	}
	public function get_all_fledge_loan(){
			$this->db->select('*,loan.created_at as created_on');
			$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
			$this->db->where('loan.loan_progress', 'ongoing');
			return $this->db->get('loan')->result_array();
	}
	public function get_all_fledge_loan_filtering($first_date,$second_date)
	{
			$this->db->select('*,loan.created_at as created_on');
			$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
			$this->db->where('loan.loan_progress', 'ongoing');
			$this->db->where('loan.created_at >=', $first_date);
			$this->db->where('loan.created_at <=', $second_date);
			return $this->db->get('loan')->result_array();
	}
	public function get_all_loan_year($oneyearback,$limityearback){
		$this->db->select('*,loan.created_at as created_on');
		$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
		$this->db->where('loan.updated_at <=', $oneyearback);
		$this->db->where('loan.updated_at >', $limityearback);
		return $this->db->get('loan')->result_array();
	}
}

?>