<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auction_model extends CI_Model{

	public function change_status()
		{	
		//echo 	$this->input->post('id');exit;

			$this->db->set('status', $this->input->post('status'));
			$this->db->where('inventory_id', $this->input->post('inventory_id'));
			$this->db->update('inventory');
		} 
	public function get_all_loan(){
			$this->db->select('*,loan.created_at as created_on,loan.updated_at as updated_on');
			$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
			$this->db->where('loan.loan_progress','auction');
			return $this->db->get('loan')->result_array();
	}

}

?>