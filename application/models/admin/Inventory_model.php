<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_model extends CI_Model{

	public function change_status()
		{	
		//echo 	$this->input->post('id');exit;

			$this->db->set('status', $this->input->post('status'));
			$this->db->where('inventory_id', $this->input->post('inventory_id'));
			$this->db->update('inventory');
		} 
	public function add_inventory($data){
			$this->db->insert('inventory', $data);
			return true;
	}
	public function get_all_inventory(){
			$this->db->select('*');
			return $this->db->get('inventory')->result_array();
	}
	public function edit_inventory($data, $id){
			$this->db->where('inventory_id', $id);
			$this->db->update('inventory', $data);
			return true;
	}
	public function get_inventory_by_id($id){
			$query = $this->db->get_where('inventory', array('inventory_id' => $id));
			return $result = $query->row_array();
		}

}

?>