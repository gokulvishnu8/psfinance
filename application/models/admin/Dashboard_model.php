<?php
	class Dashboard_model extends CI_Model{

		public function get_all_users(){
			return $this->db->count_all('ci_users');
		}
		public function get_active_users(){
			$this->db->where('is_active', 1);
			return $this->db->count_all_results('ci_users');
		}
		public function get_deactive_users(){
			$this->db->where('is_active', 0);
			return $this->db->count_all_results('ci_users');
		}
		public function auction_trigger($loan_id){
			$this->db->set('loan_progress','auction');
			$this->db->where('loan_id',$loan_id);
			$this->db->update('loan');
		}
		public function get_loan(){
			$this->db->select('*,loan.created_at as created_on,loan.updated_at as updated_on');
			$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
			$this->db->where('loan.loan_progress', 'ongoing');
			return $this->db->get('loan')->result_array();
	}
	public function get_all_loans(){
		return $this->db->count_all('loan');
	}
	public function get_all_inventory(){
		return $this->db->count_all('inventory');
	}
	public function get_all_loan_auction(){
			$this->db->select('*');
			$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
			$this->db->where('loan.loan_progress','auction');
			return $this->db->get('loan')->num_rows();
	}
	public function get_all_interestInfo(){
		$query = $this->db->get_where('interest_rate', array('id' => 1));
		return $result = $query->row_array();
	}
	public function get_loan_dashboard(){
		$this->db->select('*,loan.created_at as created_on,loan.updated_at as updated_on');
		$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
		$this->db->where('loan.loan_progress', 'ongoing');
		$this->db->order_by("loan_id", "DESC");
		$this->db->limit(5);
		return $this->db->get('loan')->result_array();
}
public function get_all_inventory_list(){
		$this->db->select('*');
		$this->db->order_by("inventory_id", "DESC");
		$this->db->limit(4);
		return $this->db->get('inventory')->result_array();
}
public function get_all_invoices(){
	$this->db->select('
			ci_payments.id,
			ci_payments.invoice_no,
			ci_users.username as client_name,
			ci_payments.payment_status,
			ci_payments.grand_total,
			ci_payments.currency,
			ci_payments.created_date,
			ci_payments.loanid,
			ci_users.firstname,
			ci_users.lastname,
			'
		);
		$this->db->from('ci_payments');
		$this->db->join('ci_users', 'ci_users.id = ci_payments.user_id ', 'Left');
		$this->db->order_by("ci_payments.id", "DESC");
		$this->db->limit(5);
		$query = $this->db->get();
	return $query->result_array();
}
public function update_interest_rate($data){
		$update_rows = array('today_gold_rate' => $data,'modified_date'=>date('Y-m-d : h:m:s'));
		$this->db->where('id', 1 );
		$this->db->update('interest_rate', $update_rows);
		return true;
}
public function ifTodaysRateUpdated(){
	$this->db->select('*');
	$this->db->where('modified_date',date('Y-m-d : h:m:s'));
	return $this->db->get('interest_rate')->num_rows();
}
	}

?>
