<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Interest_model extends CI_Model{

	public function getInterestRate(){
			$this->db->select('*');
			return $this->db->get('interest_rate')->result_array();
	}
	public function update_interest_rate($data){
			$this->db->where('id', 1);
			$this->db->update('interest_rate', $data);
			return true;
	}
}

?>