<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_model extends CI_Model{

	public function get_all_users(){
			$this->db->select('*');
			$this->db->where('is_active', 1);
			return $this->db->get('ci_users')->result_array();
	}
	public function get_all_inventory(){
			$this->db->select('*');
			return $this->db->get('inventory')->result_array();
	}
	public function add_loan($data){
			$this->db->insert('loan', $data);
			$insert_id = $this->db->insert_id();
   			return  $insert_id;
	}
	public function add_loan_inventory($loanInventorydata){
			$this->db->insert('loan_inventory_details', $loanInventorydata);
			$insert_id = $this->db->insert_id();
   			return  $insert_id;
	}
	public function get_all_loan(){
			$this->db->select('*,loan.created_at as created_on,loan.updated_at as updated_on');
			$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
			return $this->db->get('loan')->result_array();
	}
	public function get_all_ongoing_loan(){
			$this->db->select('*,loan.created_at as created_on');
			$this->db->where('loan.loan_progress','ongoing');
			$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
			return $this->db->get('loan')->result_array();
	}
	public function get_all_loan_by_id($id){
			$this->db->select('*');
			$this->db->where('loan_id', $id);
			// $this->db->join('ci_users', 'ci_users.id = loan.customer_id');
			return $this->db->get('loan')->row();
	}
	public function change_status()
		{
			$this->db->set('loan_status', $this->input->post('status'));
			$this->db->where('loan_id', $this->input->post('loan_id'));
			$this->db->update('loan');
		}
	public function get_loan_by_id($id){
			$query = $this->db->get_where('loan', array('loan_id' => $id));
			return $result = $query->row_array();
	}
	public function get_loan_inventory_id($id){
			$query = $this->db->select('*')->from('loan_inventory_details')->join('inventory', 'inventory.inventory_id = loan_inventory_details.inventory_id')->where('loan_inventory_details.loan_ID',$id)->get();
			return $result = $query->result_array();
	}
	public function update_total_amount($given,$data, $id){
		    $this->db->set('amount',$given);
		    $this->db->set('loan_amount',$data);
			$this->db->where('loan_id', $id);
			$this->db->update('loan');
			return true;
	}
	public function get_all_interestInfo(){
		$query = $this->db->get_where('interest_rate', array('id' => 1));
		return $result = $query->row_array();
	}
	public function get_all_loan_year($oneyearback){
		$this->db->select('*,loan.created_at as created_on');
		$this->db->join('ci_users', 'ci_users.id = loan.customer_id');
		$this->db->where('loan.updated_at <=', $oneyearback);
		return $this->db->get('loan')->result_array();
	}
	public function complete_payment($id, $amount, $close){
		if($close == true){
			$this->db->set('loan_progress','closed');
		}
		$this->db->set('amount',$amount);
		$this->db->set('updated_at',date("Y-m-d"));
		$this->db->where('loan_id', $id);
		$this->db->update('loan');
		return true;
	}

	public function update_amount_inventory($id, $amount, $total_price){
		$loan_inventory = $this->get_loan_inventory_id($id);
		$reduction_percentage = ($amount / $total_price) * 100;
		foreach ($loan_inventory as $key => $value) {
			$reduction_amount = $value['price'] - ($value['price'] * ($reduction_percentage / 100));
			$this->db->set('price',round($reduction_amount,2));
			$this->db->where('id', $value['id']);
			$this->db->update('loan_inventory_details');
		}
		return true;
	}
	public function add_to_payments($data){
		$this->db->insert('loan_payments', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
}

?>
